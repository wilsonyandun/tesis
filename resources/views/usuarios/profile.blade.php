@extends('layout.master')
@section('aditional_css')
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/dataTables.bootstrap.css">
@stop
@section('content')
<div class="col-lg-12">
<div class="page-content">
<div id="page-user-profile" class="row">
<div class="col-md-12">
    @if(Session::get('msg'))
    <div class="alert alert-success alert-dismissable">
        <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
        <h4 style="color: #000000">{{Session::get('msg')}} </h4>
    </div>
    @endif
<div class="row">
<div class="col-md-4">
    <div class="form-group">
        <div class="text-center mbl"><img
                src="{{env('APP_BASE')}}images/logo.jpg"
                style="border: 5px solid #fff; box-shadow: 0 2px 3px rgba(0,0,0,0.25);"
                class="img-circle"/></div>
    </div>
    <table class="table table-striped table-hover" >
        <tbody>
        <tr>
            <td width="50%">Nombres</td>
            <td>{{$usuario->nombres}}</td>
        </tr>
        <tr>
            <td width="50%">Apellidos</td>
            <td>{{$usuario->apellidos}}</td>
        </tr>
        <tr>
            <td width="50%">Usuario</td>
            <td>{{$usuario->username}}</td>
        </tr>
        <tr>
            <td width="50%">Email</td>
            <td width="50%">{{$usuario->email}}</td>
        </tr>
        <tr>
            <td width="50%">Tipo</td>
            @if($usuario->tipo==0)
            <td><span class="label label-success">Administrador General</span></td>
            @elseif($usuario->tipo==1)
            <td><span class="label label-success">Administrador de Dirección</span></td>
            @elseif($usuario->tipo==2)
            <td><span class="label label-success">Administrador de Escuela</span></td>
            @else
            <td><span class="label label-success">Usuario</span></td>
            @endif
        </tr>

        </tbody>
    </table>
</div>
<div class="col-md-8">
<ul class="nav nav-tabs ul-edit responsive">

    <li class="active"><a href="#tab-edit" data-toggle="tab"><i class="fa fa-edit"></i>&nbsp;
            Editar Usuario</a></li>

</ul>
<div id="generalTabContent" class="tab-content">
<div id="tab-edit" class="tab-pane fade in active">
<div class="row">
<div class="col-md-9">
<div class="tab-content">

    <div id="tab-profile-setting" class="tab-pane fade in active">
        {!! Form::model($usuario,['action'=>$action1,'method'=>'POST','id'=>'form_data','class'=>'form-horizontal form-bordered form-val1']) !!}
        <div class="form-body">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            Hay problemas con la información que ingresaste.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="form-group"><label
                    class="col-sm-3 control-label">Nombres</label>

                <div class="col-sm-9 controls"><input name="nombres" minlength="2" id="nombres" type="text"
                                                      placeholder="" value="{{$usuario->nombres}}"
                                                      class="form-control required "/>
                </div>
            </div>
            <div class="form-group"><label
                    class="col-sm-3 control-label">Apellidos</label>

                <div class="col-sm-9 controls"><input name="apellidos" minlength="2"
                                                      id="apellidos" type="text"
                                                      placeholder=""
                                                      value="{{$usuario->apellidos}}"
                                                      class="form-control required "/>
                </div>
            </div>
            <div class="form-group"><label
                class="col-sm-3 control-label">Email</label>

            <div class="col-sm-9 controls"><input name="email"type="email"
                                                   placeholder=""
                                                   value="{{$usuario->email}}"
                                                   class="form-control email required"/>
            </div>
            </div>



            <div class="form-group mbn"><label
                    class="col-sm-3 control-label"></label>

                <div class="col-sm-9 controls">
                    <button id="enviar1" type="submit" class="btn btn-primary" disabled>Guardar</button>
                </div>
            </div>
            </div>
        {!! Form::close() !!}
    </div>
    <div id="tab-account-setting" class="tab-pane fade">
        {!! Form::model($usuario,['action'=>$action2,'method'=>'POST','id'=>'form_password','class'=>'form-horizontal form-bordered form-val2']) !!}
            <div class="form-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    Hay problemas con la información que ingresaste.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="form-group"><label
                        class="col-sm-3 control-label">Password</label>

                    <div class="col-sm-9 controls">
                        <div class="row">
                            <div class="col-xs-6"><input minlength="7" id="password" type="password" name="password"
                                                         class="form-control required password"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group"><label
                        class="col-sm-3 control-label">Confirm
                        Password</label>

                    <div class="col-sm-9 controls">
                        <div class="row">
                            <div class="col-xs-6"><input minlength="7" type="password" name="confirm_password" equalTo="#password"
                                                         class="form-control required password" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mbn"><label
                        class="col-sm-3 control-label"></label>

                    <div class="col-sm-9 controls">
                        <button id="enviar2" type="submit" class="btn btn-primary" disabled>Guardar</button>
                    </div>
                </div>
            </div>


        {!! Form::close() !!}

    </div>

</div>
</div>
<div class="col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href="#tab-profile-setting" data-toggle="tab"><i
                    class="fa fa-folder-open"></i>&nbsp;
                Edición de Perfil</a></li>
        <li><a href="#tab-account-setting" data-toggle="tab"><i
                    class="fa fa-cogs"></i>&nbsp;
                Cambio de Contraseña </a></li>

    </ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


@stop

@section('aditional_scripts')
<script src="{{env('APP_BASE')}}vendors/jquery-validate/jquery.validate.min.js"></script>
<script>
    $(function () {
    $(".form-val1").validate({
            errorPlacement: function(error, element)
            {
                error.insertAfter(element);
            }

        }
    );
    $(".form-val2").validate({
            errorPlacement: function(error, element)
            {
                error.insertAfter(element);
            }

        }
    );
    });
    $('#enviar1').on('click', function(e) {
        $('#enviar1').attr("disabled","true");
        $('#enviar1').html("Guardando");
        $('#form_data').submit();
    });
    $('#form_data').bind('change keyup', function() {
        if($('#form_data').validate().checkForm()) {
            $('#enviar1').attr('disabled', false);
        } else {
            $('#enviar1').attr('disabled', true);
        } });
    $('#enviar2').on('click', function(e) {
        $('#enviar2').attr("disabled","true");
        $('#enviar2').html("Guardando");
        $('#form_password').submit();
    });
    $('#form_password').bind('change keyup', function() {
        if($('#form_password').validate().checkForm()) {
            $('#enviar2').attr('disabled', false);
        } else {
            $('#enviar2').attr('disabled', true);
        } });
    function goBack() {
        window.history.back();
    }
</script>

@stop