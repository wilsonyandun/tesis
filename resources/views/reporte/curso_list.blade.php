@extends('layout.master')
@section('aditional_css')
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet"
      href="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/dataTables.bootstrap.css">
@stop
@section('content')
<div class="col-lg-12">
<div class="portlet box">
<div class="portlet-header">
    <div class="caption">Lista de Alumnos </div>
</div>
<div class="portlet-body">

    <div class="row mbm">
        <div class="col-lg-12">
<h2>Nombre del Curso: {{$curso->nombre}}</h2>
            <h4>Escuela: {{$curso->escuela->nombre}}</h4>
            <h4>Fecha Inicio: {{$curso->fecha_inicio}}</h4>
            <h4>Fechan Fin: {{$curso->fecha_fin}}</h4>

<br>

</div>
        </div>
<div class="row mbm">
<div class="col-lg-12">
<div class="table-responsive">
<table id="table_id"
       class="table table-hover table-striped table-bordered table-advanced tablesorter display">
    <thead>
    <tr>

        <th width="4%">#</th>
        <th >Nombres</th>
        <th >Apellidos</th>
        <th >Identificación</th>
        <th width="2%">Sexo</th>
        <th>País</th>
        <th>Grado</th>
        <th>Arma</th>
        <th width="10%">Estado</th>
        <th>Observaciones</th>
    </tr>
    <tbody>
    @foreach($curso_personas as $curso)
    <tr>
        <td>{{++$i}}</td>
        <td>{{$curso->persona->nombres}}</td>
        <td>{{$curso->persona->apellidos}}</td>
        <td>{{$curso->persona->identificacion}}</td>
        <td>{{$curso->persona->sexo}}</td>
        <td>{{$curso->persona->nacionalidad->nombre}}</td>
        @if(count($curso->persona->grado_personas)>0 )
        <td>{{$curso->persona->grado_personas->last()->grado->nombre}}</td>
        @else
        <td></td>
        @endif
        @if(count($curso->persona->arma_personas)>0 )
        <td>{{$curso->persona->arma_personas->last()->arma->nombre}}</td>
        @else
        <td></td>
        @endif
        @if($curso->estado=="A")
        <td><span class="label label-sm label-success">Aprobado</span></td>
        @elseif($curso->estado=="O")
        <td><span class="label label-sm label-warning">Otros</span></td>
        @elseif($curso->estado=="R")
        <td><span class="label label-sm label-danger">Reprobado</span></td>
        @else
        <td><span class="label label-sm label-info">Pendiente</span></td>
        @endif
        <td>{{$curso->observaciones}}</td>

    </tr>
    @endforeach

    </tbody>
    </thead></table>
</div>
    <div class="form-actions text-right pal">
        <button onclick="goBack()"type="button" class="btn btn-green">Regresar</button>
    </div>
</div>
</div>
</div>
</div>
</div>

@stop

@section('aditional_scripts')
<script src="{{env('APP_BASE')}}vendors/DataTables/media/js/jquery.dataTables.js"></script>
<script src="{{env('APP_BASE')}}vendors/DataTables/media/js/dataTables.bootstrap.js"></script>
<script src="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script src="{{env('APP_BASE')}}js/table-datatables.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/sco.message/sco.message.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/notific8.js"></script>
<script src="{{env('APP_BASE')}}js/ui-notific8.js"></script>
<script>
    function goBack() {
        window.history.back();
    }
</script>
@stop