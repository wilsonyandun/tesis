<?php
/**
 * Created by PhpStorm.
 * User: Marketing
 * Date: 15/03/15
 * Time: 08:47 PM
 */
use Illuminate\Database\Seeder;
class EscuelaTableSeeder extends Seeder{
public function run()
{
    DB::table('escuelas')->insert(array(
        'nombre'=>'DIRECCIÓN DE EDUCACIÓN Y DOCTRINA MILITAR',
        'abreviatura'=>'DIEDMIL','direccion_id'=>'1','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'DIRECCIÓN GENERAL DE EDUCACIÓN Y DOCTRINA ',
        'abreviatura'=>'DIGEDO','direccion_id'=>'2','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'COMANDO DE EDUCACIÓN Y DOCTRINA DEL EJÉRCITO',
        'abreviatura'=>'CEDE','direccion_id'=>'3','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'COMANDO DE EDUCACIÓN Y DOCTRINA ',
        'abreviatura'=>'COED','direccion_id'=>'4','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'ESCUELA SUPERIOR MILITAR ELOY ALFARO ',
        'abreviatura'=>'ESMIL','direccion_id'=>'3','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'ESCUELA SUPERIOR MILITAR DE AVIACIÓN COSME RENELLA ',
        'abreviatura'=>'ESMA','direccion_id'=>'4','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'ESCUELA SUPERIOR NAVAL CMTE RAFAEL MORAL VALVERDE ',
        'abreviatura'=>'ESSUNA','direccion_id'=>'2','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'ESCUELA DE INFANTERIA  ',
        'abreviatura'=>'EIE','direccion_id'=>'3','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'ESCUELA DE CABALLERIA ',
        'abreviatura'=>'ESCABLIN','direccion_id'=>'3','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'ESCUELA DE ARTILLERIA ',
        'abreviatura'=>'ESCART','direccion_id'=>'3','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'ESCUELA DE INGENIERIA ',
        'abreviatura'=>'E','direccion_id'=>'3','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'ESCUELA DE COMUNICACIONES ',
        'abreviatura'=>'ESCOM','direccion_id'=>'3','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'ESCUELA CONJUNTA DE INTELIGENCIA MILITAR ',
        'abreviatura'=>'ESCIM','direccion_id'=>'1','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'ESCUELA CONJUNTA DE ARTILLERÍA ANTIAEREA ',
        'abreviatura'=>'ESCAA','direccion_id'=>'1','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'ESCUELA CONJUNTA DE MISIONES DE PAZ',
        'abreviatura'=>'UEMPE','direccion_id'=>'1','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'ESCUELA DE IWIAS ',
        'abreviatura'=>'ESIWIAS','direccion_id'=>'3','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'ESCUELA DE FORMACIÓN DE SOLDADOS ',
        'abreviatura'=>'ESFORSE','direccion_id'=>'3','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'ESCUELA DE FUERZAS ESPECIALES ',
        'abreviatura'=>'ESFE','direccion_id'=>'3','activo'=>'1'
    ));
    DB::table('escuelas')->insert(array(
        'nombre'=>'ESCUELA DE SELVA Y CONTRAINSURGENCIA ',
        'abreviatura'=>'ESCI','direccion_id'=>'3','activo'=>'1'
    ));





}
}