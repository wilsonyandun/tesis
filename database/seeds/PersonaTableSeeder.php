<?php
/**
 * Created by PhpStorm.
 * User: Marketing
 * Date: 15/03/15
 * Time: 08:47 PM
 */
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class PersonaTableSeeder extends Seeder{
public function run()
{

    $faker= Faker::create();
    for($i=0;$i<10000;$i++){
        DB::table('personas')->insert(array(
            'nombres'=>$faker->firstName." ".$faker->firstName,
            'apellidos'=>$faker->lastName." ".$faker->lastName,
            'identificacion'=>$faker->numerify('1#########'),
            'email'=>$faker->unique()->email,
            'sexo'=>$faker->randomElement($array=array ('M','F')),
            'direccion'=>$faker->address,
            'tipo_sangre'=>$faker->randomElement($array=array ('O+','O-','A+','A-','B+','B-','AB+','AB-')) ,
            'telefono'=> $faker->phoneNumber ,
            'fecha_nacimiento'=>$faker->dateTimeThisCentury->format('Y-m-d'),
            'nacionalidad_id'=>"1"
        ));
    }
    for($i=0;$i<40;$i++){
        DB::table('personas')->insert(array(
            'nombres'=>$faker->firstName." ".$faker->firstName,
            'apellidos'=>$faker->lastName." ".$faker->lastName,
            'identificacion'=>$faker->numerify('1#########'),
            'email'=>$faker->unique()->email,
            'sexo'=>$faker->randomElement($array=array ('M','F')),
            'direccion'=>$faker->address,
            'tipo_sangre'=>$faker->randomElement($array=array ('O+','O-','A+','A-','B+','B-','AB+','AB-')) ,
            'telefono'=> $faker->phoneNumber ,
            'fecha_nacimiento'=>$faker->dateTimeThisCentury->format('Y-m-d'),
            'nacionalidad_id'=>$faker->numberBetween($min = 1, $max = 254)
        ));
    }
}
}