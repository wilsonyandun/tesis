<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CursoFormacion extends Model {

    protected $table = 'cursos_formacion';

    protected $fillable = ['observaciones','institucion_destino','tipo','descripcion','curso_id'];

    public function curso()
    {
        return $this->belongsTo('App\Curso');
    }
}
