<?php
/**
 * Created by PhpStorm.
 * User: Marketing
 * Date: 15/03/15
 * Time: 08:47 PM
 */
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class GradoPersonaTableSeeder extends Seeder{
public function run()
{
    $faker= Faker::create();
    for($i=1;$i<3000;$i++){
    DB::table('grado_persona')->insert(array(
        'persona_id'=>$i,
        'grado_id'=>$faker->numberBetween($min = 1, $max = 54)
    ));
    }

}
}