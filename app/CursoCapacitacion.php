<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CursoCapacitacion extends Model {

    protected $table = 'cursos_capacitacion';

    protected $fillable = ['observaciones','institucion_destino','tipo','descripcion','curso_id'];

    public function curso()
    {
        return $this->belongsTo('App\Curso');
    }
}
