<?php namespace App\Http\Controllers;
use App\Direccion;
use App\Escuela;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Auditoria;

class EscuelaController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('admin_direccion');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function getLista($msg=null)
	{
        $title='Escuelas | Lista ';
        $page_title='Lista de Escuelas';
        $nav1='Escuelas';
        $nav2='Lista';
        $cursos=new Collection();
        if(Auth::user()->tipo=='0'){
        $escuelas=Escuela::all();
        }elseif(Auth::user()->tipo=='1'){
            $direccion=Auth::user()->escuela->direccion()->get();
            $escuelas=Escuela::where('direccion_id','=',$direccion[0]->id)->get();
            foreach($escuelas as $escuela){
                $cursos=$cursos->merge(Curso::where("escuela_id", '=', $escuela->id)->get());
            }
        }
		return view('escuela.list',array('msg'=>$msg,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'escuelas'=>$escuelas
        ));
	}
    public function getNuevo(){
        $title='Escuelas | Crear ';
        $page_title='Creación de Escuela';
        $nav1='Escuelas';
        $nav2='Crear';
        $action='EscuelaController@postNuevo';
        $escuela=new Escuela();
        if(Auth::user()->tipo=='0'){
        $direcciones=Direccion::where('activo','1');
        }elseif(Auth::user()->tipo=='1'){
            $direcciones=Auth::user()->escuela->direccion()->get();
        }
        $direcciones=$direcciones->lists('abreviatura','id');
        return view('escuela.form',array('action'=>$action,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'escuela'=>$escuela,'direcciones'=>$direcciones
        ));
    }
    public function postNuevo(Request $request){
        $data=$request->all();
        $direcciones=Direccion::where('activo','1');
        $direcciones=$direcciones->lists('id');
        $direcciones=implode(',',$direcciones);
        $rules= array(
            'nombre'=>'required|unique:direcciones',
            'abreviatura'=>'required',
            'direccion_id'=>'required|numeric|in:'.$direcciones);
        $v=Validator::make($data,$rules);
        if($v->fails()){
           return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
            DB::enableQueryLog();
        $escuela= Escuela::create($request->all());
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"CREAR ESCUELA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
        return redirect()->back()->with('msg','creada');
        }
    }
    public function getEditar($id){
        $title='Escuelas | Editar ';
        $page_title='Edición de Escuela';
        $nav1='Escuelas';
        $nav2='Crear';
        $action='EscuelaController@postEditar';
        $escuela=Escuela::find($id);
        if(Auth::user()->tipo=='0'){
            $direcciones=Direccion::where('activo','1');
        }elseif(Auth::user()->tipo=='1'){
            $direcciones=Auth::user()->escuela->direccion()->get();
        }
        $direcciones=$direcciones->lists('abreviatura','id');
        return view('escuela.form',array('action'=>$action,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'escuela'=>$escuela,'direcciones'=>$direcciones
        ));
    }
    public function postEditar(Request $request){
        $data=$request->all();
        $rules= array(
            'nombre'=>'required',
            'abreviatura'=>'required');
        $v=Validator::make($data,$rules);
        if($v->fails()){
            return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
            $escuela=Escuela::find($request['escuela_id']);
            DB::enableQueryLog();
            $escuela->update($request->all());
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"EDITAR ESCUELA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
        return redirect()->back()->with('msg','modificada');
        }
    }
    public function postEliminar(Request $request){
        $escuela=Escuela::find($request['id']);
        $data=array("activo"=>"0");
        DB::enableQueryLog();
        $escuela->update($data);
        $queries = DB::getQueryLog();
        $last_query = end($queries);
        $last_query=implode(" , ", array_flatten($last_query));
        DB::disableQueryLog();
        $info=array("ip"=>$request->getClientIp(),"accion"=>"ELIMINAR ESCUELA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
        $auditoria=Auditoria::create($info);
        $validate=true;
        echo json_encode(array('validate'=>$validate));
    }


}
