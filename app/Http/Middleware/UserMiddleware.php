<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        if(Auth::user()->tipo!=0&&Auth::user()->tipo!=1&&Auth::user()->tipo!=2&&Auth::user()->tipo!=3){
            Auth::logout();
            return redirect()->guest('auth/login')->withErrors("Usted no tiene permisos para ingresar a esta opción");
        }
		return $next($request);
	}

}
