<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CursoPersona extends Model {

    protected $table = 'curso_persona';

    protected $fillable = ['observaciones','estado','persona_id','curso_id'];

    public function persona()
    {
        return $this->belongsTo('App\Persona');
    }
    public function curso(){
        return $this->belongsTo('App\Curso');
    }
    public function scopeReprobados($query)
    {
        return $query->whereEstado('R');
    }
    public function scopeAprobados($query)
    {
        return $query->whereEstado('A');
    }
    public function scopeOtros($query)
    {
        return $query->whereEstado('O');
    }
}
