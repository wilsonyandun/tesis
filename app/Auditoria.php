<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Auditoria extends Model {

    protected $table = 'auditoria';

    protected $fillable = ['ip','accion','sql','usuario_id'];

    public function arma_persona()
    {
        return $this->hasMany('App\ArmaPersona');
    }

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
