<?php
/**
 * Created by PhpStorm.
 * User: Marketing
 * Date: 15/03/15
 * Time: 08:47 PM
 */
use Illuminate\Database\Seeder;
class UnidadTableSeeder extends Seeder{
public function run()
{
    DB::table('unidades')->insert(array(
        'nombre'=>'BRIGADA DE INFANTERÍA Nº 13 "PICHINCHA"',
        'abreviatura'=>'13 BI','activo'=>'1'
    ));
    DB::table('unidades')->insert(array(
        'nombre'=>'BRIGADA DE INFANTERÍA Nº 1 "EL ORO"',
        'abreviatura'=>'1 BI','activo'=>'1'
    ));
    DB::table('unidades')->insert(array(
        'nombre'=>'BRIGADA DE INFANTERÍA Nº 5 "GUAYAS"',
        'abreviatura'=>'5 BI','activo'=>'1'
    ));
    DB::table('unidades')->insert(array(
        'nombre'=>'BRIGADA DE FUERZAS ESPECIALES Nº 9 "PATRIA"',
        'abreviatura'=>'9 BFE','activo'=>'1'
    ));
    DB::table('unidades')->insert(array(
        'nombre'=>'BRIGADA DE CABALLERÍA BLINDADA Nº 11 "GALÁPAGOS"',
        'abreviatura'=>'11 BCB','activo'=>'1'
    ));

    DB::table('unidades')->insert(array(
        'nombre'=>'BRIGADA DE INFANTERÍANº 7 "LOJA"',
        'abreviatura'=>'7 BCB','activo'=>'1'
    ));
    DB::table('unidades')->insert(array(
        'nombre'=>'TERCERA  DIVISIÓN DE EJÉRCITO "TARQUI" ',
        'abreviatura'=>'II DE','activo'=>'1'
    ));
    DB::table('unidades')->insert(array(
        'nombre'=>'PRIEMRA DIVISIÓN DE EJÉRCITO "SHYRIS"',
        'abreviatura'=>'I DE','activo'=>'1'
    ));
    DB::table('unidades')->insert(array(
        'nombre'=>'CUARTA DIVISIÓN DE EJÉRCITO "AMAZONAS"',
        'abreviatura'=>'IV DE','activo'=>'1'
    ));

}
}