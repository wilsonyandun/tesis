@extends('layout.master')
@section('aditional_css')
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet"
      href="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/dataTables.bootstrap.css">
@stop
@section('content')
<div class="col-lg-12">
<div class="portlet box">
<div class="portlet-header">
    <div class="caption">Lista de Direcciones</div>
</div>
<div class="portlet-body">
    @if(isset($msg))
    <div class="alert alert-success alert-dismissable">
        <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
        <h4 style="color: #000000">El usuario a sido eliminado exitosamente</h4>
    </div>
    @endif

<div class="row mbm">
<div class="col-lg-12">
<div class="table-responsive">
<table id="table_id"
       class="table table-hover table-striped table-bordered table-advanced tablesorter display">
<thead>
<tr>

    <th width="9%">#</th>
    <th>Nombre</th>
    <th width="10%">Abreviatura</th>
    <th width="10%">Estado</th>
    <th width="12%">Acciones</th>
</tr>
<tbody>
@foreach($direcciones as $direccion)
<tr>
    <td>{{++$i}}</td>
    <td>{{$direccion->nombre}}</td>
    <td>{{$direccion->abreviatura}}</td>
    @if($direccion->activo==1)
    <td><span class="label label-sm label-success">Activo</span></td>
    <td>
    <a href="{{env('APP_BASE')}}direccion/editar/{{$direccion->id}}" type="button" class="btn btn-default btn-xs mbs"><i class="fa fa-edit"></i>&nbsp;
        Editar
    </a>
    &nbsp;
    <button data-id="{{$direccion->id}}" data-toggle="modal" type="button" class="confirm-delete btn btn-danger btn-xs mbs"><i class="fa fa-trash-o"></i>&nbsp;
        Eliminar
    </button>
    </td>
    @else
    <td><span class="label label-sm label-warning">Inactivo</span></td>
    <td></td>
    @endif

</tr>
@endforeach
</tbody>
</thead></table>
</div>
</div>
</div>
</div>
</div>
</div>
<div id="modal-confirm" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">Are you sure?</div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button id="btnYes" type="button" data-dismiss="modal" class="btn btn-primary">Ok</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('aditional_scripts')
<script src="{{env('APP_BASE')}}vendors/DataTables/media/js/jquery.dataTables.js"></script>
<script src="{{env('APP_BASE')}}vendors/DataTables/media/js/dataTables.bootstrap.js"></script>
<script src="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script src="{{env('APP_BASE')}}js/table-datatables.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/sco.message/sco.message.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/notific8.js"></script>
<script src="{{env('APP_BASE')}}js/ui-notific8.js"></script>
<script>
    $('.confirm-delete').on('click', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        $('#modal-confirm').data('id', id).modal('show');
    });
    $('#btnYes').click(function() {
        // handle deletion here
        var id = $('#modal-confirm').data('id');
        var token ="{{csrf_token()}}";
        $.post("{{URL::to('/')}}/direccion/eliminar",{ id: id, _token:token },
     function(json){
       if(json.validate==true){
           window.location="{{URL::to('/')}}/direccion/lista/ok";
       }

        }
            ,"json");
        $('#modal-confirm').modal('hide');
    });
</script>

@stop