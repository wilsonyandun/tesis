@extends('layout.master')
@section('aditional_css')
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/dataTables.bootstrap.css">
@stop
@section('content')
<div class="col-lg-12">
<div class="portlet box">
<div class="portlet-header">
    <div class="caption">Información de Persona</div>
</div>
<div class="portlet-body">


<div class="row mbm">
    <div class="text-center mbl"><img
            src="{{env('APP_BASE')}}images/logo.jpg"
            style="border: 5px solid #fff; box-shadow: 0 2px 3px rgba(0,0,0,0.25);"
            class="img-circle"/></div>

<div class="col-lg-12">
    <form action="#" class="form-horizontal" >
        <div class="form-body pal"><h3>Datos Personales</h3>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"><label for="inputFirstName"
                                                   class="col-md-3 control-label"><b>
                            Nombres:</b></label>

                        <div class="col-md-9"><p
                                class="form-control-static">{{$persona->nombres}}</p></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group"><label for="inputLastName"
                                                   class="col-md-3 control-label"><b>Apellidos:</b></label>

                        <div class="col-md-9"><p
                                class="form-control-static">{{$persona->apellidos}}</p></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"><label for="inputEmail"
                                                   class="col-md-3 control-label"><b>Identificación:</b></label>

                        <div class="col-md-9">
                            <p
                                class="form-control-static">{{$persona->identificacion}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group"><label for="selGender"
                                                   class="col-md-3 control-label"><b>Sexo:</b></label>

                        <div class="col-md-9"><p
                                class="form-control-static">

                                @if($persona->sexo=="M")
                                Masculino
                                @else
                                Femenino
                                @endif
                            </p></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"><label for="inputBirthday"
                                                   class="col-md-3 control-label">Fecha de Nacimiento</label>

                        <div class="col-md-9"><p
                                class="form-control-static">{{$persona->fecha_nacimiento}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group"><label for="inputEmail"
                                                   class="col-md-3 control-label">Email:</label>

                        <div class="col-md-9"><p
                                class="form-control-static"><a
                                    href="mailto:{{$persona->email}}">{{$persona->email}}</a>
                            </p></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"><label for="inputBirthday"
                                                   class="col-md-3 control-label">Nacionalidad</label>

                        <div class="col-md-9"><p
                                class="form-control-static">{{$persona->nacionalidad->nombre}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group"><label for="inputPhone"
                                                   class="col-md-3 control-label">Tipo de Sangre</label>

                        <div class="col-md-9"><p
                                class="form-control-static">{{$persona->tipo_sangre}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <h3>Datos de Domicilio</h3>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"><label for="inputAddress1"
                                                   class="col-md-3 control-label">Dirección</label>

                        <div class="col-md-9"><p
                                class="form-control-static">{{$persona->direccion}}</p></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group"><label for="inputAddress2"
                                                   class="col-md-3 control-label">Teléfono</label>

                        <div class="col-md-9"><p
                                class="form-control-static">{{$persona->telefono}}</p></div>
                    </div>
                </div>
            </div>
            @if(count($persona->grado_personas)>0)
            <h3>Datos Militares</h3>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"><label for="inputAddress1"
                                                   class="col-md-3 control-label">Grado</label>

                        <div class="col-md-9"><p
                                class="form-control-static">{{$persona->grado_personas->last()->grado->nombre}}</p></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group"><label for="inputAddress2"
                                                   class="col-md-3 control-label">Arma</label>

                        <div class="col-md-9"><p
                                class="form-control-static">{{$persona->arma_personas->last()->arma->nombre}}</p></div>
                    </div>
                </div>
            </div>
            @endif
            @if(count($persona->titulos)>0)
            <h3>Títulos</h3>
            @foreach($persona->titulos as $titulo)
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table id="table_id"
                               class="table table-hover table-striped table-bordered table-advanced tablesorter display">
                    <thead>
                    <tr>
                    <th>Tipo</th>
                    <th>Nombre</th>
                    <th>Institución</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td>{{$titulo->tipo}}</td>
                    <td>{{$titulo->nombre}}</td>
                    <td>{{$titulo->institucion}}</td>
                    </tr>
                    </tbody>
                </table>
                    </div>
                    </div>
            </div>
            @endforeach
            @endif


        </div>
        <div class="form-actions text-right pal">
        <button onclick="goBack()"type="button" class="btn btn-green">Regresar</button>
            </div>
    </form>
</div>
</div>

</div>
</div>
</div>


@stop

@section('aditional_scripts')
@if(isset($personas))
<script src="{{env('APP_BASE')}}vendors/DataTables/media/js/jquery.dataTables.js"></script>
<script src="{{env('APP_BASE')}}vendors/DataTables/media/js/dataTables.bootstrap.js"></script>
<script src="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script src="{{env('APP_BASE')}}js/table-datatables.js"></script>
@endif
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/sco.message/sco.message.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/notific8.js"></script>
<script src="{{env('APP_BASE')}}js/ui-notific8.js"></script>
<script>
    function goBack() {
        window.history.back();
    }
</script>

@stop