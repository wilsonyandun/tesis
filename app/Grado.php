<?php namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Grado extends Model {

    protected $table = 'grados';
    protected $fillable = ['nombre','abreviatura','activo','fuerza_id'];


    public function fuerza()
    {
        return $this->belongsTo('App\Fuerza');
    }
    public static function todos(){
        return $grados = DB::table('grados')
            ->join('fuerzas', 'fuerzas.id', '=', 'grados.fuerza_id')
            ->select('grados.id as id', DB::raw('CONCAT(grados.nombre, " - ", fuerzas.nombre) AS nombre'))
            ->get();
    }

    public function grado_persona()
    {
        return $this->hasMany('App\GradoPersona');
    }


}
