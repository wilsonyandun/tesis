<?php
/**
 * Created by PhpStorm.
 * User: Marketing
 * Date: 15/03/15
 * Time: 08:47 PM
 */
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class CursoPersonaTableSeeder extends Seeder{
public function run()
{
    $faker= Faker::create();
    for($i=0;$i<300;$i++){
    DB::table('curso_persona')->insert(array(
        'persona_id'=>$faker->numberBetween($min = 1, $max = 3040),
        'curso_id'=>$faker->numberBetween($min = 1, $max = 22),
        'observaciones'=>$faker->sentence($nbWords = 6)
    ));
    }

}
}