<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CursoPerfeccionamiento extends Model {

    protected $table = 'cursos_perfeccionamiento';

    protected $fillable = ['observaciones','institucion_destino','tipo','descripcion','curso_id'];

    public function curso()
    {
        return $this->belongsTo('App\Curso');
    }
}
