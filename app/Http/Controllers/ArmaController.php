<?php namespace App\Http\Controllers;
use App\Arma;
use App\Auditoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ArmaController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('admin_direccion');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function getLista($msg=null)
	{
        $title='Armas | Lista ';
        $page_title='Lista de Armas';
        $nav1='Armas';
        $nav2='Lista';
        $armas=Arma::all();
		return view('arma.list',array('msg'=>$msg,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'armas'=>$armas
        ));
	}
    public function getNuevo(){
        $title='Armas | Crear ';
        $page_title='Crear Arma';
        $nav1='Armas';
        $nav2='Crear';
        $action='ArmaController@postNuevo';
        $arma=new Arma();
        return view('arma.form',array('action'=>$action,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'arma'=>$arma
        ));
    }
    public function postNuevo(Request $request){
        $data=$request->all();
        $rules= array(
            'nombre'=>'required|unique:armas',
            'abreviatura'=>'required');
        $v=Validator::make($data,$rules);
        if($v->fails()){
           return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
        DB::enableQueryLog();
        $arma= Arma::create($request->all());
        $queries = DB::getQueryLog();
        $last_query = end($queries);
        $last_query=implode(" , ", array_flatten($last_query));
        DB::disableQueryLog();
        $info=array("ip"=>$request->getClientIp(),"accion"=>"CREAR ARMA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
        $auditoria=Auditoria::create($info);
        return redirect()->back()->with('msg','creada');
        }
    }
    public function getEditar($id){
        $title='Armas | Editar ';
        $page_title='Edición de Dirección';
        $nav1='Armas';
        $nav2='Crear';
        $action='ArmaController@postEditar';
        $arma=Arma::find($id);
        return view('arma.form',array('action'=>$action,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'arma'=>$arma
        ));
    }
    public function postEditar(Request $request){
        $data=$request->all();
        $rules= array(
            'nombre'=>'required|unique:armas,nombre,'.$request['arma_id'],
            'abreviatura'=>'required');
        $v=Validator::make($data,$rules);
        if($v->fails()){
            return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
        $arma=Arma::find($request['arma_id']);
        DB::enableQueryLog();
        $arma->update($request->all());
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"MODIFICAR ARMA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
        return redirect()->back()->with('msg','modificado');
        }
    }
    public function postEliminar(Request $request){
        $arma=Arma::find($request['id']);
        $data=array("activo"=>"0");
        DB::enableQueryLog();
        $arma->update($data);
        $queries = DB::getQueryLog();
        $last_query = end($queries);
        $last_query=implode(" , ", array_flatten($last_query));
        DB::disableQueryLog();
        $info=array("ip"=>$request->getClientIp(),"accion"=>"ELIMINAR ARMA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
        $auditoria=Auditoria::create($info);
        $validate=true;
        echo json_encode(array('validate'=>$validate));
    }


}
