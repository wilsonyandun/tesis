<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model {

    protected $table = 'personas';

    protected $fillable = ['nombres','apellidos','identificacion','email','sexo','direccion','tipo_sangre','telefono','fecha_nacimiento','activo','nacionalidad_id'];


    public function titulos()
    {
        return $this->hasMany('App\Titulo');
    }
    public function grado_personas()
    {
        return $this->hasMany('App\GradoPersona');
    }
    public function curso_personas()
    {
        return $this->hasMany('App\CursoPersona');
    }
    public function arma_personas()
    {
        return $this->hasMany('App\ArmaPersona');
    }
    public function unidad_personas()
    {
        return $this->hasMany('App\UnidadPersona');
    }
    public function nacionalidad()
    {
        return $this->belongsTo('App\Nacionalidad');
    }

    public function scopeMujeres($query)
    {
        return $query->whereSexo('F');
    }
    public function scopeHombres($query)
    {
        return $query->whereSexo('M');
    }
    public function scopeNacionales($query)
    {
        return $query->whereNacionalidadId('1');
    }
    public function grados()
    {
        return $this->belongsToMany('App\Grado', 'grado_persona', 'persona_id', 'grado_id');
    }
    public function armas()
    {
        return $this->belongsToMany('App\Arma', 'arma_persona', 'persona_id', 'arma_id');
    }




}
