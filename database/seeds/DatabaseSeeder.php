<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
         $this->call('DireccionTableSeeder');
        $this->call('EscuelaTableSeeder');
        $this->call('NacionalidadTableSeeder');
		 $this->call('UsuarioTableSeeder');
         $this->call('FuerzasTableSeeder');
         $this->call('GradosTableSeeder');
         $this->call('ArmasTableSeeder');
         $this->call('UnidadTableSeeder');
        $this->call('PersonaTableSeeder');
        $this->call('CursosTableSeeder');
        $this->call('CursoPersonaTableSeeder');
        $this->call('GradoPersonaTableSeeder');
        $this->call('ArmaPersonaTableSeeder');

	}

}
