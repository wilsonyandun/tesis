<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UnidadPersona extends Model {

    protected $table = 'unidad_persona';
    protected $fillable = ['unidad_id','persona_id'];

    public function persona()
    {
        return $this->belongsTo('App\Persona');
    }
    public function unidad(){
        return $this->belongsTo('App\Unidad');
    }
}
