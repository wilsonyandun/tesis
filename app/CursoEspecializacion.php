<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CursoEspecializacion extends Model {

    protected $table = 'cursos_especializacion';

    protected $fillable = ['observaciones','institucion_destino','tipo','descripcion','curso_id'];

    public function curso()
    {
        return $this->belongsTo('App\Curso');
    }
}
