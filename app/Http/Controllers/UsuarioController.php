<?php namespace App\Http\Controllers;
use App\Escuela;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Auditoria;
use Illuminate\Support\Facades\Hash;
class UsuarioController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('admin');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function getLista($msg=null)
	{
        $title='Usuarios | Lista ';
        $page_title='Lista de Usuarios';
        $nav1='Usuarios';
        $nav2='Lista';
        $usuarios=User::all();
        $tipos=array('0'=>'Administrador General','1'=>'Administrador de Dirección','2'=>'Administrador de Escuela','3'=>'Usuario');
		return view('usuarios.list',array('tipos'=>$tipos,'msg'=>$msg,  'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'usuarios'=>$usuarios
        ));
	}
    public function getNuevo(){
        $title='Usuarios | Crear ';
        $page_title='Crear de Usuario';
        $nav1='Usuarios';
        $nav2='Crear';
        $action='UsuarioController@postNuevo';
        $tipos=array('0'=>'Administrador General','1'=>'Administrador de Dirección','2'=>'Administrador de Escuela','3'=>'Usuario');
        $usuario=new User();
        $escuelas=Escuela::where('activo','1');;
        $escuelas=$escuelas->lists('abreviatura','id');
        return view('usuarios.form',array('escuelas'=>$escuelas,'tipos'=>$tipos,'action'=>$action,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'usuario'=>$usuario
        ));
    }
    public function postNuevo(Request $request){
        $data=$request->all();
        $rules= array(
            'nombres'=>'required',
            'apellidos'=>'required',
            'email'=>'required|email',
            'username'=>'required|unique:usuarios',
            'tipo'=>'required');
        $v=Validator::make($data,$rules);
        if($v->fails()){
            return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
            DB::enableQueryLog();
            $usuario= User::create($request->all());
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"CREAR PERSONA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
            return redirect()->back()->with('msg','creado');
        }
    }
    public function getEditar($id){
        $title='Usuarios | Editar ';
        $page_title='Edición de Usuario';
        $nav1='Usuarios';
        $nav2='Crear';
        $action='UsuarioController@postEditar';
        $usuario=User::find($id);
        $escuelas=Escuela::all();
        $estados=array('1'=>'Activo','0'=>'Inactivo');
        $escuelas=$escuelas->lists('abreviatura','id');
        $tipos=array('0'=>'Administrador General','1'=>'Administrador de Dirección','2'=>'Administrador de Escuela','3'=>'Usuario');
        return view('usuarios.form',array('estados'=>$estados,'escuelas'=>$escuelas,'tipos'=>$tipos,'action'=>$action,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'usuario'=>$usuario
        ));
    }
    public function postEditar(Request $request){
        $data=$request->all();
        $rules= array(
            'nombres'=>'required',
            'apellidos'=>'required',
            'email'=>'required|email',
            'username'=>'required|unique:usuarios,username,'.$request['usuario_id'],
            'tipo'=>'required|digits:1');
        $v=Validator::make($data,$rules);
        if($v->fails()){
            return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
            $usuario=User::find($request['usuario_id']);
            DB::enableQueryLog();
            $usuario->update($request->all());
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"MODIFICAR PERSONA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
            return redirect()->back()->with('msg','modificado');
        }
    }
    public function postEliminar(Request $request){
        $usuario=User::find($request['id']);
        $data=array("activo"=>"0");
        DB::enableQueryLog();
        $usuario->update($data);
        $queries = DB::getQueryLog();
        $last_query = end($queries);
        $last_query=implode(" , ", array_flatten($last_query));
        DB::disableQueryLog();
        $info=array("ip"=>$request->getClientIp(),"accion"=>"ELIMINAR PERSONA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
        $auditoria=Auditoria::create($info);
        $validate=true;
        echo json_encode(array('validate'=>$validate));
    }

}
