@extends('layout.master')
@section('aditional_css')
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet"
      href="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/dataTables.bootstrap.css">
@stop
@section('content')
<div class="col-lg-12">
<div class="portlet box">
<div class="portlet-header">
    <div class="caption">Lista de Títulos </div>
</div>
<div class="portlet-body">

<div class="row mbm">
<div class="col-lg-12">
<div class="table-responsive">
<table id="table_id"
       class="table table-hover table-striped table-bordered table-advanced tablesorter display">
    <thead>
    <tr>

        <th width="4%">#</th>
        <th>Nombres</th>
        <th>Apellidos</th>
        <th>Identificación</th>
        <th>Tipo</th>
        <th>Nombre</th>
        <th>Institución</th>
    </tr>
    <tbody>
    @foreach($titulos as $titulo)
    <tr>
        <td>{{++$i}}</td>
        <td>{{$titulo->persona->nombres}}</td>
        <td>{{$titulo->persona->apellidos}}</td>
        <td>{{$titulo->persona->identificacion}}</td>
        <td>{{$titulo->tipo}}</td>
        <td>{{$titulo->nombre}}</td>
        <td>{{$titulo->institucion}}</td>
    </tr>
    @endforeach

    </tbody>
    </thead></table>
</div>

</div>
</div>
</div>
</div>
</div>

@stop

@section('aditional_scripts')
<script src="{{env('APP_BASE')}}vendors/DataTables/media/js/jquery.dataTables.js"></script>
<script src="{{env('APP_BASE')}}vendors/DataTables/media/js/dataTables.bootstrap.js"></script>
<script src="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script src="{{env('APP_BASE')}}js/table-datatables.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/sco.message/sco.message.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/notific8.js"></script>
<script src="{{env('APP_BASE')}}js/ui-notific8.js"></script>
<script>
    function goBack() {
        window.history.back();
    }
</script>
@stop