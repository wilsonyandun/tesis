@extends('layout.master')
@section('aditional_css')
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/select2/select2-madmin.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-select/bootstrap-select.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/multi-select/css/multi-select-madmin.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/animate.css/animate.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/jquery-pace/pace.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/iCheck/skins/all.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-daterangepicker/daterangepicker-bs3.css">
@stop
@section('content')

<div class="col-lg-12">
<div class="panel panel-blue">
    <div class="panel-heading">{{$page_title}}</div>
    <div class="panel-body pan">

        @if(Session::get('msg'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
            <h4 style="color: #000000">El usuario a sido {{Session::get('msg')}} exitosamente</h4>
        </div>
        @endif
        {!! Form::model($usuario,['action'=>$action,'method'=>'POST','id'=>'form_data','class'=>'form-horizontal form-bordered form-validate']) !!}
        @if(isset($usuario->id))
        <input name="usuario_id" type="hidden" value="{{$usuario->id}}">
        @endif
            <div class="form-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    Hay problemas con la información que ingresaste.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="form-group">

                    <label for="nombres"
                                               class="col-md-3 control-label">
                        Nombres <span class='require'>*</span></label>

                    <div class="col-md-4"><input name="nombres" minlength="2" id="nombres" type="text"
                                                 placeholder="" value="{{$usuario->nombres}}"
                                                 class="form-control required "/><span
                            class="help-block">Ej: Juan Esteban</span></div>
                </div>
                <div class="form-group"><label for="apellidos"
                                               class="col-md-3 control-label">
                        Apellidos <span class='require'>*</span></label>

                    <div class="col-md-4"><input name="apellidos" minlength="2"
                                                 id="apellidos" type="text"
                                                 placeholder=""
                                                 value="{{$usuario->apellidos}}"
                                                 class="form-control required "/><span
                            class="help-block">Ej: Torres Vivar </span></div>
                </div>
                <div class="form-group"><label for="username"
                                               class="col-md-3 control-label">
                        Usuario <span class='require'>*</span></label>

                    <div class="col-md-4"><input name="username" id="username" type="text"
                                                 placeholder=""
                                                 value="{{$usuario->username}}"
                                                 class="form-control required "/><span
                            class="help-block">Ej: jetorres </span></div>
                </div>
                <div class="form-group"><label for="email"
                                               class="col-md-3 control-label">
                        Email <span class='require'>*</span></label>

                    <div class="col-md-4">
                        @if($usuario->email!="")
                        <input name="email"type="email"
                                                 placeholder=""
                                                 value="{{$usuario->email}}"
                                                 class="form-control email required"/>
                        @else
                        <input name="email"type="email"
                               placeholder=""
                               value="@"
                               class="form-control email required"/>
                        @endif
                        <span
                            class="help-block">Ej: jetorres@diedmil.mil.ec </span></div>
                </div>
                <div class="form-group"><label for="password"
                                               class="col-md-3 control-label">
                        Password <span class='require'>*</span></label>

                    <div class="col-md-4"><input name="password" id="password" type="password"
                                                 placeholder=""

                                                 class="form-control  password"/><span
                            class="help-block"> </span></div>
                </div>

                <div class="form-group"><label for="tipo"
                                               class="col-md-3 control-label">
                        TIPO <span class='require'>*</span></label>

                    <div class="col-md-6 col-sm-6 col-xs-6">{!!Form::select('tipo', $tipos, $usuario->tipo,array("class"=>"select2-size form-control")) !!}
                        <span class="help-block"></span></div>
                </div>

                <div class="form-group"><label for="escuela_id"
                                               class="col-md-3 control-label">
                        ESCUELA <span class='require'>*</span></label>

                    <div class="col-md-6 col-sm-6 col-xs-6">{!!Form::select('escuela_id', $escuelas, $usuario->escuela_id,array("class"=>"select2-size form-control")) !!}
                        <span
                            class="help-block"></span></div>
                </div>
                @if(isset($estados))
                <div class="form-group"><label for="activo"
                                               class="col-md-3 control-label">
                        Estado <span class='require'>*</span></label>

                    <div class="col-md-6 col-sm-6 col-xs-6">{!!Form::select('activo', $estados, $usuario->activo,array("class"=>"select2-size form-control")) !!}
                        <span
                            class="help-block"></span></div>
                </div>
                @endif
            </div>
            <div class="form-actions text-right pal">
                <button id="enviar" type="submit" class="btn btn-primary" disabled>Guardar</button>
                &nbsp;
                <button type="button" class="btn btn-green">Cancelar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
</div>

@stop
@section('aditional_scripts')
<script>
    $('#enviar').on('click', function(e) {
        $('#enviar').attr("disabled","true");
        $('#enviar').html("Guardando");
        $('#form_data').submit();
    });
    $('#form_data').bind('change keyup', function() {
        if($('#form_data').validate().checkForm()) {
            $('#enviar').attr('disabled', false);
        } else {
            $('#enviar').attr('disabled', true);
        } });


</script>
<script src="{{env('APP_BASE')}}vendors/jquery-validate/jquery.validate.min.js"></script>
<script src="{{env('APP_BASE')}}js/form-validation.js"></script>
<script src="{{env('APP_BASE')}}vendors/select2/select2.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/multi-select/js/jquery.multi-select.js"></script>
<script src="{{env('APP_BASE')}}js/ui-dropdown-select.js"></script>
@stop