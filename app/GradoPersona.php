<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GradoPersona extends Model {

    protected $table = 'grado_persona';
    protected $fillable = ['grado_id','persona_id'];

    public function persona()
    {
        return $this->belongsTo('App\Persona');
    }
    public function grado(){
        return $this->belongsTo('App\Grado');
    }
    public function scopeGrado($query, $type)
    {
        return $query->whereGradoId($type);
    }
}
