<?php namespace App\Http\Controllers;
use App\Direccion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Auditoria;
use Illuminate\Support\Facades\DB;

class DireccionController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('admin');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function getLista($msg=null)
	{
        $title='Direcciones | Lista ';
        $page_title='Lista de Direcciones';
        $nav1='Direcciones';
        $nav2='Lista';
        $direcciones=Direccion::all();
		return view('direccion.list',array('msg'=>$msg,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'direcciones'=>$direcciones
        ));
	}
    public function getNuevo(){
        $title='Direcciones | Crear ';
        $page_title='Crear de Dirección';
        $nav1='Direcciones';
        $nav2='Crear';
        $action='DireccionController@postNuevo';
        $direccion=new Direccion();
        return view('direccion.form',array('action'=>$action,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'direccion'=>$direccion
        ));
    }
    public function postNuevo(Request $request){
        $data=$request->all();
        $rules= array(
            'nombre'=>'required|unique:direcciones',
            'abreviatura'=>'required');
        $v=Validator::make($data,$rules);
        if($v->fails()){
           return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
            DB::enableQueryLog();
            $direccion= Direccion::create($request->all());
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"CREAR DIRECCION","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
        return redirect()->back()->with('msg','creado');
        }
    }
    public function getEditar($id){
        $title='Direcciones | Editar ';
        $page_title='Edición de Dirección';
        $nav1='Direcciones';
        $nav2='Crear';
        $action='DireccionController@postEditar';
        $direccion=Direccion::find($id);
        return view('direccion.form',array('action'=>$action,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'direccion'=>$direccion
        ));
    }
    public function postEditar(Request $request){
        $data=$request->all();
        $rules= array(
            'nombre'=>'required|unique:direcciones,nombre,'.$request['direccion_id'],
            'abreviatura'=>'required');
        $v=Validator::make($data,$rules);
        if($v->fails()){
            return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
        $direccion=Direccion::find($request['direccion_id']);
            DB::enableQueryLog();
        $direccion->update($request->all());
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"EDITAR DIRECCION","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
        return redirect()->back()->with('msg','modificado');
        }
    }
    public function postEliminar(Request $request){
        $direccion=Direccion::find($request['id']);
        $data=array("activo"=>"0");
        DB::enableQueryLog();
        $direccion->update($data);
        $queries = DB::getQueryLog();
        $last_query = end($queries);
        $last_query=implode(" , ", array_flatten($last_query));
        DB::disableQueryLog();
        $info=array("ip"=>$request->getClientIp(),"accion"=>"ELIMINAR DIRECCION","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
        $auditoria=Auditoria::create($info);
        $validate=true;
        echo json_encode(array('validate'=>$validate));
    }


}
