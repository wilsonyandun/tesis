<?php namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class AdminDireccionMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        if(Auth::user()->tipo!=0&&Auth::user()->tipo!=1){
            Auth::logout();
            return redirect()->guest('auth/login')->withErrors("Usted no tiene permisos para ingresar a esta opción");
        }
		return $next($request);
	}

}
