<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('grados', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre');
            $table->string('abreviatura');
            $table->integer('fuerza_id')->unsigned();
            $table->foreign('fuerza_id')->references('id')->on('fuerzas');
            $table->boolean('activo')->default('1');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('grados');
	}

}
