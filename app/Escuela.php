<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Escuela extends Model {

    protected $table = 'escuelas';
    protected $fillable = ['nombre','abreviatura','ciudad','direccion','nombre_director','telefono','activo','direccion_id'];

    public function users()
    {
        return $this->hasMany('App\User');
    }
    public function direccion()
    {
        return $this->belongsTo('App\Direccion');
    }
    public function cursos()
    {
        return $this->hasMany('App\Curso');
    }

}
