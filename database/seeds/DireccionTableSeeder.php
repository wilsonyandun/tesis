<?php
/**
 * Created by PhpStorm.
 * User: Marketing
 * Date: 15/03/15
 * Time: 08:47 PM
 */
use Illuminate\Database\Seeder;
class DireccionTableSeeder extends Seeder{
public function run()
{
    DB::table('direcciones')->insert(array(
        'nombre'=>'DIRECCIÓN DE EDUCACIÓN Y DOCTRINA MILITAR',
        'abreviatura'=>'DIEDMIL','activo'=>'1'
    ));
    DB::table('direcciones')->insert(array(
        'nombre'=>'DIRECCIÓN GENERAL DE EDUCACIÓN Y DOCTRINA ',
        'abreviatura'=>'DIGEDO','activo'=>'1'
    ));
    DB::table('direcciones')->insert(array(
        'nombre'=>'COMANDO DE EDUCACIÓN Y DOCTRINA DEL EJÉRCITO',
        'abreviatura'=>'CEDE','activo'=>'1'
    ));
    DB::table('direcciones')->insert(array(
        'nombre'=>'COMANDO DE EDUCACIÓN Y DOCTRINA ',
        'abreviatura'=>'COED','activo'=>'1'
    ));

}
}