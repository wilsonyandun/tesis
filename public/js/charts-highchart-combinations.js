$(function () {

    /* Column, line and pie */
    $('#column-line-and-pie').highcharts({
        chart: {

        },
        title: {
            text: 'Reporte Gráfico'
        },
        xAxis: {
            categories: ['Personas']
        },
        tooltip: {
            formatter: function() {
                var s;
                if (this.point.name) { // the pie chart
                    s = ''+
                        this.point.name +': '+ this.y ;
                } else {
                    s = ''+
                        this.x  +': '+ this.y;
                }
                return s;
            }
        },
        labels: {
            items: [{
                html: 'Total personas',
                style: {
                    left: '40px',
                    top: '8px',
                    color: 'black'
                }
            }]
        },
        series: [{
            type: 'column',
            name: 'Hombres',
            data: [30]


        }, {
            type: 'column',
            name: 'Mujeres',
            data: [ 6]


        }, {
            type: 'pie',
            name: 'Total Personas',
            data: [{
                name: 'Hombres',
                y: 30,
                color: Highcharts.getOptions().colors[4] // Jane's color
            }, {
                name: 'Mujeres',
                y: 6,
                color: Highcharts.getOptions().colors[2] // John's color
            }, ],
            center: [40, 80],
            size: 120,
            showInLegend: false,
            dataLabels: {
                enabled: true
            }
        }]
    });

});
