<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonaAlumno extends Model {

    protected $table = 'persona_alumno';

    protected $fillable = ['persona_id'];

}
