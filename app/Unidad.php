<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidad extends Model {

    protected $table = 'unidades';

    protected $fillable = ['nombre','abreviatura','activo'];

    public function unidad_persona()
    {
        return $this->hasMany('App\UnidadPersona');
    }
}
