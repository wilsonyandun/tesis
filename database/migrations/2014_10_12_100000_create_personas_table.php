<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('personas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombres');
            $table->string('apellidos');
			$table->string('identificacion')->unique();
            $table->string('email')->unique();
            $table->string('sexo','1');
            $table->string('direccion');
            $table->string('tipo_sangre','5');
            $table->string('telefono','20');
            $table->date('fecha_nacimiento');
            $table->integer('nacionalidad_id')->unsigned();
            $table->foreign('nacionalidad_id')->references('id')->on('nacionalidades');
            $table->boolean('activo')->default('1');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('personas');
	}

}
