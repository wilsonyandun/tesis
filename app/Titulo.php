<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Titulo extends Model {

    protected $table = 'titulos';

    protected $fillable = ['tipo','nombre','institucion','persona_id'];
    public function persona()
    {
        return $this->belongsTo('App\Persona');
    }

}
