<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Nacionalidad extends Model {

    protected $table = 'nacionalidades';

    protected $fillable = ['nombre'];

    public function cursos()
    {
        return $this->hasMany('App\Curso');
    }
    public function personas()
    {
        return $this->hasMany('App\Persona');
    }

}
