@extends('layout.master')
@section('aditional_css')
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/animate.css/animate.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/jquery-pace/pace.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/iCheck/skins/all.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/iCheck/skins/all.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-colorpicker/css/colorpicker.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-datepicker/css/datepicker.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-daterangepicker/daterangepicker-bs3.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-clockface/css/clockface.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-switch/css/bootstrap-switch.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/select2/select2-madmin.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-select/bootstrap-select.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/multi-select/css/multi-select-madmin.css">
@stop
@section('content')

<div class="col-lg-12">
<div class="panel panel-blue">
    <div class="panel-heading">{{$page_title}}</div>
    <div class="panel-body pan">

        @if(Session::get('msg'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
            <h4 style="color: #000000">El usuario a sido {{Session::get('msg')}} exitosamente</h4>
        </div>
        @endif
        {!! Form::model($persona,['action'=>$action,'method'=>'POST','id'=>'form_data','class'=>'form-horizontal form-bordered form-validate']) !!}
        @if(isset($persona->id))
        <input name="persona_id" type="hidden" value="{{$persona->id}}">
        @endif
            <div class="form-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    Hay problemas con la información que ingresaste.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="form-group">

                    <label for="nombres"
                                               class="col-md-3 control-label">
                        Nombres <span class='require'>*</span></label>

                    <div class="col-md-6"><input name="nombres" minlength="2" id="nombres" type="text"
                                                 placeholder="" value="{{$persona->nombres}}"
                                                 class="form-control required "/><span
                            class="help-block">Ej: JOSE MANUEL </span></div>
                </div>
                <div class="form-group">

                    <label for="apellidos"
                           class="col-md-3 control-label">
                        Apellidos <span class='require'>*</span></label>

                    <div class="col-md-6"><input name="apellidos" minlength="2" id="apellidos" type="text"
                                                 placeholder="" value="{{$persona->apellidos}}"
                                                 class="form-control required "/><span
                            class="help-block">Ej: CASTILLO TORRES </span></div>
                </div>
                <div class="form-group">

                    <label for="identificacion"
                           class="col-md-3 control-label">
                        CC/Pasaporte <span class='require'>*</span></label>

                    <div class="col-md-6"><input name="identificacion" minlength="2" id="identificacion" type="text"
                                                 placeholder="" value="{{$persona->identificacion}}"
                                                 class="form-control required "/><span
                            class="help-block">Ej: 1001244259 </span></div>
                </div>
                <div class="form-group">

                    <label for="email"
                           class="col-md-3 control-label">
                        Email<span class='require'>*</span></label>

                    <div class="col-md-6"><input name="email" minlength="2" id="email" type="email"
                                                 placeholder="" value="{{$persona->email}}"
                                                 class="form-control required email"/><span
                            class="help-block">Ej: jose.castillo@espe.edu.ec </span></div>
                </div>
                <div class="form-group">

                    <label for="direccion"
                           class="col-md-3 control-label">
                        Dirección <span class='require'>*</span></label>

                    <div class="col-md-8"><input name="direccion" minlength="2" id="direccion" type="text"
                                                 placeholder="" value="{{$persona->direccion}}"
                                                 class="form-control required "/><span
                            class="help-block">Ej: Joel Monroy OE9-280 y Río Cenepa </span></div>
                </div>
                <div class="form-group">

                    <label for="telefono"
                           class="col-md-3 control-label">
                        Teléfono <span class='require'>*</span></label>

                    <div class="col-md-6"><input name="telefono" minlength="2" id="telefono" type="text"
                                                 placeholder="" value="{{$persona->telefono}}"
                                                 class="form-control required "/><span
                            class="help-block">Ej: 2073112 - 0984312183 </span></div>
                </div>
                <div class="form-group">

                    <label for="sexo"
                           class="col-md-3 control-label">
                        Sexo <span class='require'>*</span></label>

                    <div id="square-skin" class="col-md-4 skin skin-square"><ul class="list-unstyled">
                            <li class="pbm">

                                {!! Form::radio('sexo', 'M',true) !!}
                                &nbsp;&nbsp;<label
                                    for="square-radio-1">Masculino</label></li>
                                {!! Form::radio('sexo', 'F') !!}
                                &nbsp;&nbsp;<label
                                    for="square-radio-2">Femenino</label></li>
                        </ul></div>
                </div>

                <div class="form-group">

                    <label for="tipo"
                           class="col-md-3 control-label">
                        Tipo <span class='require'>*</span></label>

                    <div id="square-skin" class="col-md-4 skin skin-square">
                <ul class="list-unstyled">
                    <li class="pbm">
                        {!! Form::checkbox('tipo[]', 'alumno',true,['class'=>'tipo ','id'=>'alumno']) !!}
                        &nbsp;&nbsp;<label
                            for="square-checkbox-1">Alumno</label>
                    </li>
                    <li class="pbm">
                        {!! Form::checkbox('tipo[]', 'docente',false,['class'=>'tipo ','id'=>'docente']) !!}
                        &nbsp;&nbsp;<label
                            for="square-checkbox-2">Docente</label></li>
                    <li class="pbm">
                        {!! Form::checkbox('tipo[]', 'administrativo',false,['class'=>'tipo','id'=>'administrativo']) !!}
                        &nbsp;&nbsp;<label
                            for="square-checkbox-3">Administrativo</label></li>

                </ul>
                        </div>
                    </div>
                <div class="form-group" id="info" style="display: none">

                    <label for="tipo_docente"
                           class="col-md-3 control-label">
                        Tipo de Docente <span class='require'>*</span></label>

                    <div id="square-skin" class="col-md-4 skin skin-square"><ul class="list-unstyled">
                            <li class="pbm">

                                {!! Form::radio('tipo_docente', 'completo',true) !!}
                                &nbsp;&nbsp;<label
                                    for="square-radio-1">Tiempo Completo</label></li>
                            {!! Form::radio('tipo_docente', 'parcial',false) !!}
                            &nbsp;&nbsp;<label
                                for="square-radio-2">Tiempo Parcial</label></li>
                        </ul></div>
                </div>
                <div class="form-group">
                    <label for="fecha_nacimiento"
                           class="col-md-3 control-label">
                        Fecha de Nacimiento <span class='require'>*</span></label>

                    <div class="col-md-3"><input name="fecha_nacimiento" type="text" data-date-format="yyyy-mm-dd"
                                                 placeholder="yyyy-mm-dd" value="{{$persona->fecha_nacimiento}}"
                                                 class="datepicker-default form-control required" style="z-index: 100"/>
                        <span
                            class="help-block">Ej: 1985-12-15</span>
                    </div>
                </div>
                <div class="form-group"><label for="grado_id"
                                               class="col-md-3 control-label">
                        Nacionalidad <span class='require'>*</span></label>

                    <div class="col-md-6 col-sm-6 col-xs-6">{!!Form::select('nacionalidad_id', $nacionalidades, $persona->nacionalidad_id,array("class"=>"select2-size form-control")) !!}
                        <span
                            class="help-block"></span></div>
                </div>
                <div class="form-group">
                    <label for="civi_militar"
                           class="col-md-3 control-label">
                        Militar/Civil <span class='require'>*</span></label>

                    <div class="col-md-4">
                        <div data-on-label="Militar" data-off-label="Civil" data-text-label=""
                                               class="make-switch"><input id="militar" name="civil_militar"type="checkbox" checked="checked"
                                                                          class="switch"/></div>
                    </div>
                </div>
                <div id="info_mil">
                    <div class="form-group"><label for="grado_id"
                                                   class="col-md-3 control-label">
                            Grado <span class='require'>*</span></label>
@if(count($persona->grado_persona)>0)
                        <div class="col-md-6 col-sm-6 col-xs-6">{!!Form::select('grado_id', $grados, $persona->grado_persona->last()->grado_id,array("class"=>"select2-size select2-militar form-control")) !!}
                        @else
                            <div class="col-md-6 col-sm-6 col-xs-6">{!!Form::select('grado_id', $grados, "",array("class"=>"select2-size select2-militar form-control")) !!}
                            @endif
                            <span
                            class="help-block"></span></div>
                    </div>
                    <div class="form-group"><label for="arma_id"
                                                   class="col-md-3 control-label">
                            Arma <span class='require'>*</span></label>

                        <div class="col-md-6 col-sm-6 col-xs-6">{!!Form::select('arma_id', $armas, " ",array("class"=>"select2-size select2-militar form-control")) !!}
                        <span
                            class="help-block"></span></div>
                    </div>
                    <div class="form-group"><label for="arma_id"
                                                   class="col-md-3 control-label">
                            Unidad de Origen <span class='require'>*</span></label>

                        <div class="col-md-6 col-sm-6 col-xs-6">{!!Form::select('unidad_id', $unidades, " ",array("class"=>"select2-size select2-militar form-control")) !!}
                        <span
                            class="help-block"></span></div>
                    </div>
                </div>

            </div>
                <div class="form-group"><label for="arma_id"
                                               class="col-md-3 control-label">
                        Título</label>

                    <div id="divContenedorTabla" class="col-md-9">
                        <table class="table table-hover table-striped table-bordered table-advanced tablesorter display">

                            <thead>
                            <tr>
                                <th>Tipo</th>
                                <th>Nombre Título</th>
                                <th>Institución</th>
                                <th width="22">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>

                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="4" align="right">
                                    <input type="button" value="Agregar una fila" class="clsAgregarFila">
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            <div class="form-actions text-right pal">
                <button id="enviar" class="btn btn-primary" disabled>Guardar</button>
                &nbsp;
                <button type="button" class="btn btn-green">Cancel</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
</div>

@stop
@section('aditional_scripts')

<script src="{{env('APP_BASE')}}vendors/iCheck/icheck.js"></script>
<script src="{{env('APP_BASE')}}vendors/iCheck/custom.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-validate/jquery.validate.min.js"></script>
<script src="{{env('APP_BASE')}}js/form-validation.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="{{env('APP_BASE')}}vendors/moment/moment.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-clockface/js/clockface.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-maskedinput/jquery-maskedinput.js"></script>
<script src="{{env('APP_BASE')}}vendors/charCount.js"></script>
<script src="{{env('APP_BASE')}}js/form-components.js"></script>
<script src="{{env('APP_BASE')}}vendors/select2/select2.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/multi-select/js/jquery.multi-select.js"></script>
<script src="{{env('APP_BASE')}}js/ui-dropdown-select.js"></script>
<script>
    $('input[name="tipo_docente"]').val("");
    $('#docente').on('ifChanged', function(event){
        if($('#docente').is(':checked')){
            $('#info').show();
            $('input[name="tipo_docente"]').val("completo");
        }else
        {
            $('#info').hide();
            $('#tipo_docente').iCheck('uncheck');
            $('input[name="tipo_docente"]').val("");
        }
    });
    $('#militar').on('change', function(e) {
    if($('#militar').is(':checked')){
        $('#info_mil').show();
        var $example=$('.select2-militar');
        $example.val("1").trigger("change");

    }else{
        var $example=$('.select2-militar');
        $('#info_mil').hide();
        $example.val(null).trigger("change");
    }

    });
    $('#enviar').on('click', function(e) {
        if ($('input[name="tipo[]"]').is(':checked')) {
            $('#enviar').attr("disabled","true");
            $('#enviar').html("Guardando");
            $('#form_data').submit();
        }
        else {
            alert('Se debe seleccionar un tipo');
            $('#form_data').submit(false);

        }
    });
    $('#form_data').bind('change keyup', function() {
        if($('#form_data').validate().checkForm()) {
            $('#enviar').attr('disabled', false);
        } else {
            $('#enviar').attr('disabled', true);
        } });

    //evento que se dispara al hacer clic en el boton para agregar una nueva fila
    $(document).on('click','.clsAgregarFila',function(){
        //almacenamos en una variable todo el contenido de la nueva fila que deseamos
        //agregar. pueden incluirse id's, nombres y cualquier tag... sigue siendo html
        var strNueva_Fila='<tr>'+
            '<td><select name="tipo_titulo[]" class="select2-size form-control">'+
            '<option value="tecnologo">TECNÓLOGO</option>'+
            '<option value="licenciado">LICENCIADO</option>'+
            '<option value="ingeniero">INGENIERO</option>'+
            '<option value="magister">MAGISTER</option>'+
            '<option value="phd">PHD</option>'+
            '<option value="medico">MÉDICO</option>'+
            '<option value="abogado">ABOGADO</option>'+
            '<option value="arquitecto">ARQUITECTO</option>'+
            '</select></td>'+
            '<td><textarea name="nombre_titulo[]" minlength="2" type="text" placeholder="" class="form-control required "/></td>'+
            '<td><textarea name="institucion_titulo[]" minlength="2" type="text" placeholder="" class="form-control required " /></td>'+
            '<td align="right"><input type="button" value="-" class="clsEliminarFila"></td>'+
            '</tr>';

        /*obtenemos el padre del boton presionado (en este caso queremos la tabla, por eso
                 utilizamos get(3)
                     table -> padre 3
                         tfoot -> padre 2
                             tr -> padre 1
                                 td -> padre 0
                 nosotros queremos utilizar el padre 3 para agregarle en la etiqueta
                 tbody una nueva fila*/
        var objTabla=$(this).parents().get(3);

        //agregamos la nueva fila a la tabla
        $(objTabla).find('tbody').append(strNueva_Fila);

        //si el cuerpo la tabla esta oculto (al agregar una nueva fila) lo mostramos
        if(!$(objTabla).find('tbody').is(':visible')){
            //le hacemos clic al titulo de la tabla, para mostrar el contenido
            $(objTabla).find('caption').click();
        }
    });

    //cuando se haga clic en cualquier clase .clsEliminarFila se dispara el evento
    $(document).on('click','.clsEliminarFila',function(){
        /*obtener el cuerpo de la tabla; contamos cuantas filas (tr) tiene
                 si queda solamente una fila le preguntamos al usuario si desea eliminarla*/
        var objCuerpo=$(this).parents().get(2);
        if($(objCuerpo).find('tr').length==1){
            if(!confirm('Esta es el única fila de la lista ¿Desea eliminarla?')){
                return;
            }
        }


        var objFila=$(this).parents().get(1);

        $(objFila).remove();
    });
</script>
@stop