<?php
/**
 * Created by PhpStorm.
 * User: Marketing
 * Date: 15/03/15
 * Time: 08:47 PM
 */
use Illuminate\Database\Seeder;
class GradosTableSeeder extends Seeder{
public function run()
{
    DB::table('grados')->insert(array(
        'nombre'=>'GENERAL DE EJÉRCITO', 'abreviatura'=> 'GRAE', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'GENERAL DE DIVISIÓN', 'abreviatura'=> 'GRAD', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'GENERAL DE BRIGADA', 'abreviatura'=> 'GRAB', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'CORONEL', 'abreviatura'=> 'CRNL', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'TENIENTE CORONEL', 'abreviatura'=> 'TCRN', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'MAYOR', 'abreviatura'=> 'MAYO', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'CAPITÁN', 'abreviatura'=> 'CAPT', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'TENIENTE', 'abreviatura'=> 'TNTE', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SUBTENIENTE', 'abreviatura'=> 'SUBT', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SUBOFICIAL MAYOR', 'abreviatura'=> 'SUBM', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SUBOFICIAL PRIMERO', 'abreviatura'=> 'SUBP', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SUBOFICIAL SEGUNDO', 'abreviatura'=> 'SUBS', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SARGENTO PRIMERO', 'abreviatura'=> 'SGOP', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SARGENTO SEGUNDO', 'abreviatura'=> 'SGOS', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SARGENTO SEGUNDO', 'abreviatura'=> 'SGOS', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'CABO PRIMERO', 'abreviatura'=> 'CBOP', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'CABO SEGUNDO', 'abreviatura'=> 'CBOS', 'fuerza_id'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SOLDADO', 'abreviatura'=> 'SLDO', 'fuerza_id'=>'1'
    ));



    DB::table('grados')->insert(array(
        'nombre'=>'ALMIRANTE', 'abreviatura'=> 'ALM', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'VICEALMIRANTE', 'abreviatura'=> 'VALM', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'CONTRALMIRANTE', 'abreviatura'=> 'CALM', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'CAPITÁN DE NAVÍO', 'abreviatura'=> 'CPNV', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'CAPITÁN DE FRAGATA', 'abreviatura'=> 'CPFG', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'CAPITÁN DE CORBETA', 'abreviatura'=> 'CPCB', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'TENIENTE DE NAVÍO', 'abreviatura'=> 'TNNV', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'TENIENTE DE FRAGATA', 'abreviatura'=> 'TNFG', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'ALFEREZ DE FRAGATA', 'abreviatura'=> 'ALFG', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SUBOFICIAL MAYOR', 'abreviatura'=> 'SUBM', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SUBOFICIAL PRIMERO', 'abreviatura'=> 'SUBP', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SUBOFICIAL SEGUNDO', 'abreviatura'=> 'SUBS', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SARGENTO PRIMERO', 'abreviatura'=> 'SGOP', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SARGENTO SEGUNDO', 'abreviatura'=> 'SGOS', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SARGENTO SEGUNDO', 'abreviatura'=> 'SGOS', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'CABO PRIMERO', 'abreviatura'=> 'CBOP', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'CABO SEGUNDO', 'abreviatura'=> 'CBOS', 'fuerza_id'=>'2'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'MARINERO', 'abreviatura'=> 'MARO', 'fuerza_id'=>'2'
    ));



    DB::table('grados')->insert(array(
        'nombre'=>'GENERAL DEL AIRE', 'abreviatura'=> 'GRAA', 'fuerza_id'=>'3'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'TENIENTE GENERAL', 'abreviatura'=> 'TNTG', 'fuerza_id'=>'3','activo'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'BRIGADIER GENERAL', 'abreviatura'=> 'BRIG', 'fuerza_id'=>'3','activo'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'CORONEL', 'abreviatura'=> 'CRNL', 'fuerza_id'=>'3','activo'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'TENIENTE CORONEL', 'abreviatura'=> 'TCRN', 'fuerza_id'=>'3','activo'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'MAYOR', 'abreviatura'=> 'MAYO', 'fuerza_id'=>'3','activo'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'CAPITÁN', 'abreviatura'=> 'CAPT', 'fuerza_id'=>'3','activo'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'TENIENTE', 'abreviatura'=> 'TNTE', 'fuerza_id'=>'3','activo'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SUBTENIENTE', 'abreviatura'=> 'SUBT', 'fuerza_id'=>'3','activo'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SUBOFICIAL MAYOR', 'abreviatura'=> 'SUBM', 'fuerza_id'=>'3','activo'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SUBOFICIAL PRIMERO', 'abreviatura'=> 'SUBP', 'fuerza_id'=>'3','activo'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SUBOFICIAL SEGUNDO', 'abreviatura'=> 'SUBS', 'fuerza_id'=>'3','activo'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SARGENTO PRIMERO', 'abreviatura'=> 'SGOP', 'fuerza_id'=>'3','activo'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SARGENTO SEGUNDO', 'abreviatura'=> 'SGOS', 'fuerza_id'=>'3','activo'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SARGENTO SEGUNDO', 'abreviatura'=> 'SGOS', 'fuerza_id'=>'3','activo'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'CABO PRIMERO', 'abreviatura'=> 'CBOP', 'fuerza_id'=>'3','activo'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'CABO SEGUNDO', 'abreviatura'=> 'CBOS', 'fuerza_id'=>'3','activo'=>'1'
    ));
    DB::table('grados')->insert(array(
        'nombre'=>'SOLDADO', 'abreviatura'=> 'SLDO', 'fuerza_id'=>'3','activo'=>'1'
    ));

}
}