<?php namespace App\Http\Controllers;
use App\Fuerza;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Auditoria;

class FuerzaController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('admin_direccion');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function getLista($msg=null)
	{
        $title='Fuerzas | Lista ';
        $page_title='Lista de Fuerzas';
        $nav1='Fuerzas';
        $nav2='Lista';
        $fuerzas=Fuerza::all();
		return view('fuerza.list',array('msg'=>$msg,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'fuerzas'=>$fuerzas
        ));
	}
    public function getNuevo(){
        $title='Fuerzas | Crear ';
        $page_title='Crear Fuerza';
        $nav1='Fuerzas';
        $nav2='Crear';
        $action='FuerzaController@postNuevo';
        $fuerza=new Fuerza();
        return view('fuerza.form',array('action'=>$action,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'fuerza'=>$fuerza
        ));
    }
    public function postNuevo(Request $request){
        $data=$request->all();
        $direcciones=Direccion::where('activo','1');
        $direcciones=$direcciones->lists('id');
        $direcciones=implode(',',$direcciones);
        $rules= array(
            'nombre'=>'required|unique:escuelas',
            'abreviatura'=>'required');
        $v=Validator::make($data,$rules);
        if($v->fails()){
           return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
            DB::enableQueryLog();
        $fuerza= Fuerza::create($request->all());
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"CREAR FUERZA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
        return redirect()->back()->with('msg','creado');
        }
    }
    public function getEditar($id){
        $title='Fuerzas | Editar ';
        $page_title='Edición de Fuerza';
        $nav1='-Fuerzas';
        $nav2='Crear';
        $action='FuerzaController@postEditar';
        $fuerza=Fuerza::find($id);
        return view('fuerza.form',array('action'=>$action,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'fuerza'=>$fuerza
        ));
    }
    public function postEditar(Request $request){
        $data=$request->all();
        $rules= array(
            'nombre'=>'required',
            'abreviatura'=>'required');
        $v=Validator::make($data,$rules);
        if($v->fails()){
            return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
            $fuerza=Fuerza::find($request['fuerza_id']);
            DB::enableQueryLog();
            $fuerza->update($request->all());
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"EDITAR FUERZA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
        return redirect()->back()->with('msg','modificado');
        }
    }
    public function postEliminar(Request $request){
        $fuerza=Fuerza::find($request['id']);
        $data=array("activo"=>"0");
        DB::enableQueryLog();
        $fuerza->update($data);
        $queries = DB::getQueryLog();
        $last_query = end($queries);
        $last_query=implode(" , ", array_flatten($last_query));
        DB::disableQueryLog();
        $info=array("ip"=>$request->getClientIp(),"accion"=>"ELIMINAR FUERZA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
        $auditoria=Auditoria::create($info);
        $validate=true;
        echo json_encode(array('validate'=>$validate));
    }


}
