<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model {

    protected $table = 'cursos';

    protected $fillable = ['nombre','fecha_inicio','fecha_fin','costo','ciudad','pais_id','activo','escuela_id'];
    public function escuela()
    {
        return $this->belongsTo('App\Escuela');
    }
    public function alumnos()
    {
        return $this->belongsToMany('App\Persona', 'curso_persona', 'curso_id', 'persona_id');
    }
    public function curso_persona()
    {
        return $this->hasMany('App\CursoPersona');
    }
    public function curso_capacitacion(){
        return $this->hasOne('App\CursoCapacitacion');
    }
    public function curso_educacion_regular(){
        return $this->hasOne('App\CursoEducacionRegular');
    }
    public function curso_perfeccionamiento(){
        return $this->hasOne('App\CursoPerfeccionamiento');
    }
    public function curso_especializacion(){
        return $this->hasOne('App\CursoEspecializacion');
    }
    public function curso_formacion(){
        return $this->hasOne('App\CursoFormacion');
    }
    public function pais(){
        return $this->belongsTo('App\Nacionalidad','pais_id');
    }


}
