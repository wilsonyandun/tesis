@extends('layout.master')
@section('aditional_css')
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/dataTables.bootstrap.css">
@stop
@section('content')
<div class="col-lg-12">
<div class="portlet box">
<div class="portlet-header">
    <div class="caption">Lista de Personas</div>
</div>
<div class="portlet-body">
    {!! Form::model($persona,['action'=>$action,'method'=>'POST','id'=>'form_data','class'=>'form-horizontal form-bordered form-validate']) !!}
    <div class="form-body">

        <div class="form-group">

            <label for="nombres"
                   class="col-md-2 control-label">
                Nombres </label>

            <div class="col-md-4"><input name="nombres"  id="nombres" type="text"
                                         placeholder="" value="{{$persona->nombres}}"
                                         class="form-control  "/></div>
            <label for="apellidos"
                   class="col-md-2 control-label">
                Apellidos</label>

            <div class="col-md-4"><input name="apellidos" id="apellidos" type="text"
                                         placeholder=""
                                         value="{{$persona->apellidos}}"
                                         class="form-control  "/></div>

            <label for="identificacion"
                   class="col-md-2 control-label">
                Identificación</label>

            <div class="col-md-4"><input name="identificacion" id="identificacion" type="text"
                                         placeholder=""
                                         value="{{$persona->identificacion}}"
                                         class="form-control  "/></div>
            <div class="col-md-2 control-label"></div>
            <div class="col-md-4 ">
                <button id="enviar" type="submit" class="btn btn-primary col-md-4">Enviar</button>
                &nbsp;
            </div>

        </div>
        </div>
        {!! Form::close() !!}
@if(isset($personas))
<div class="row mbm">
<div class="col-lg-12">
<div class="table-responsive">
<table id="table_id"
       class="table table-hover table-striped table-bordered table-advanced tablesorter display">
<thead>
<tr>

    <th width="5%">#</th>
    <th width="15%">Nombres</th>
    <th width="15%">Apellidos</th>
    <th width="10%">Identificación</th>
    <th width="10%">Grado</th>
    <th width="10%">Arma</th>
    <th width="10%">Estado</th>
    <th width="12%">Acciones</th>
</tr>
<tbody>
@foreach($personas as $persona)
<tr>
    <td>{{++$i}}</td>
    <td>{{$persona->nombres}}</td>
    <td>{{$persona->apellidos}}</td>
    <td>{{$persona->identificacion}}</td>
    @if(count($persona->grado_personas)>0 )
    <td>{{$persona->grado_personas->last()->grado->nombre}}</td>
    @else
    <td></td>
    @endif
    @if(count($persona->arma_personas)>0 )
    <td>{{$persona->arma_personas->last()->arma->nombre}}</td>
    @else
    <td></td>
    @endif
    @if($persona->activo==1)
    <td><span class="label label-sm label-success">Activo</span></td>
    <td>
        <a href="{{env('APP_BASE')}}reporte/ver/{{$persona->id}}" type="button" class="btn btn-default btn-xs mbs"><i class="fa fa-search"></i>&nbsp;
            Ver
        </a>
    </td>
    @else
    <td><span class="label label-sm label-warning">Inactivo</span></td>
    <td></td>
    @endif

</tr>
@endforeach

</tbody>
</thead></table>
</div>
</div>
</div>

@endif

</div>
</div>
</div>


@stop

@section('aditional_scripts')
@if(isset($personas))
<script src="{{env('APP_BASE')}}vendors/DataTables/media/js/jquery.dataTables.js"></script>
<script src="{{env('APP_BASE')}}vendors/DataTables/media/js/dataTables.bootstrap.js"></script>
<script src="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script src="{{env('APP_BASE')}}js/table-datatables.js"></script>
@endif
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/sco.message/sco.message.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/notific8.js"></script>
<script src="{{env('APP_BASE')}}js/ui-notific8.js"></script>



@stop