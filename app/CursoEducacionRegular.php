<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CursoEducacionRegular extends Model {

    protected $table = 'cursos_educacion_regular';

    protected $fillable = ['observaciones','institucion_destino','tipo','descripcion','curso_id'];

    public function curso()
    {
        return $this->belongsTo('App\Curso');
    }
}
