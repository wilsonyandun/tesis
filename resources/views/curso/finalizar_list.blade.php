@extends('layout.master')
@section('aditional_css')
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-switch/css/bootstrap-switch.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/select2/select2-madmin.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-select/bootstrap-select.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/multi-select/css/multi-select-madmin.css">
@stop
@section('content')
<div class="col-lg-12">
<div class="portlet box">
<div class="portlet-header">
    <div class="caption">Lista de Alumnos</div>
</div>
<div class="portlet-body">
    @if(isset($msg))
    <div class="alert alert-success alert-dismissable">
        <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
        <h4 style="color: #000000">{{$msg}}</h4>
    </div>
    @endif
    <div class="row mbm">
        <div class="col-lg-12">
<h2>Nombre del Curso: {{$curso->nombre}}</h2>
<br>
</div>
        </div>

<div class="row mbm">
<div class="col-lg-12">
<div class="table-responsive">
<table
       class="table table-hover table-striped table-bordered display">
    <thead>
    <tr>

        <th width="5%">#</th>
        <th width="10%">Nombres</th>
        <th width="10%">Apellidos</th>
        <th width="10%">Identificación</th>
        <th width="10%">Estado</th>
        <th width="20%">Observaciones</th>
    </tr>
    <tbody>
    {!! Form::open(array('action'=>$action,'method'=>'POST','id'=>'form_data','class'=>'form-horizontal form-bordered form-validate')) !!}
    {!! Form::hidden('curso_id', $curso->id) !!}
    @foreach($curso->alumnos as $persona)
    {!! Form::hidden('persona_id[]', $persona->id) !!}
    <tr>
        <td>{{++$i}}</td>
        <td>{{$persona->nombres}}</td>
        <td>{{$persona->apellidos}}</td>
        <td>{{$persona->identificacion}}</td>
        <td><div class="col-md-12 col-sm-12 col-xs-12">{!!Form::select('estados[]', $estados, "",array("class"=>"select2-size form-control")) !!}</div></td>
        <td>
        <textarea name="observaciones[]"  id="observaciones" type="textarea" rows="2"
                      placeholder="" value=""
                      class="form-control  "/></textarea>
        </td>


    </tr>
    @endforeach



    </tbody>
    </thead></table>

    <div class="form-actions text-right pal">
        @if(count($curso->alumnos)>0)
        <button data-toggle="modal" class="btn btn-primary">Guardar</button>
        &nbsp;
        @endif
        <a href="{{env('APP_BASE')}}curso/finalizar" type="button" class="btn btn-green">Regresar</a>
    </div>
    {!! Form::close() !!}
</div>
</div>
</div>
</div>
</div>
</div>
<div id="modal-confirm" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">Are you sure?</div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button id="btnYes" type="submit" data-dismiss="modal" class="btn btn-primary">Ok</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('aditional_scripts')


<script src="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/sco.message/sco.message.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/notific8.js"></script>
<script src="{{env('APP_BASE')}}js/ui-notific8.js"></script>
<script src="{{env('APP_BASE')}}vendors/select2/select2.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/multi-select/js/jquery.multi-select.js"></script>
<script src="{{env('APP_BASE')}}js/ui-dropdown-select.js"></script>



@stop