<?php
/**
 * Created by PhpStorm.
 * User: Marketing
 * Date: 15/03/15
 * Time: 08:47 PM
 */
use Illuminate\Database\Seeder;
class ArmasTableSeeder extends Seeder{
public function run()
{
    DB::table('armas')->insert(array(
        'nombre'=>'INFANTERÍA', 'abreviatura'=> 'INF','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'CABALLERÍA BLINDADA', 'abreviatura'=> 'C.B','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'ARTILLERÍA', 'abreviatura'=> 'ART','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'INGENIERÍA', 'abreviatura'=> 'E','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'COMUNICACIONES', 'abreviatura'=> 'COM','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'INTELIGENCIA MILITAR', 'abreviatura'=> 'I.M','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'AVIACIÓN DEL EJÉRCITO', 'abreviatura'=> 'A.E','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'INTENDENCIA', 'abreviatura'=> 'INT','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'MATERIAL DE GUERRA', 'abreviatura'=> 'M.G','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'TRANSPORTES', 'abreviatura'=> 'TRP','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'SANIDAD', 'abreviatura'=> 'SND','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'JUSTICIA', 'abreviatura'=> 'JUS','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'ESPECIALISTA', 'abreviatura'=> 'ESP','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'ESTADO MAYOR', 'abreviatura'=> 'E.M','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'ESTADO MAYOR CONJUNTO', 'abreviatura'=> 'E.M.C','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'SANIDAD', 'abreviatura'=> 'SND','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'SUPERFICIE', 'abreviatura'=> 'SU','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'SUBMARINOS', 'abreviatura'=> 'SS','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'GUARDACOSTAS', 'abreviatura'=> 'GC','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'HIDROGRAFÍA', 'abreviatura'=> 'HI','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'INFANTERÍA DE MARINA', 'abreviatura'=> 'IM','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'ADMINISTRATIVO', 'abreviatura'=> 'AD','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'INFORMÁTICO', 'abreviatura'=> 'IF','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'ABASTECIMIENTOS', 'abreviatura'=> 'AB','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'AVIACIÓN NAVAL', 'abreviatura'=> 'AV','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'PILOTO', 'abreviatura'=> 'PLTO','activo'=>'1'
    ));
    DB::table('armas')->insert(array(
        'nombre'=>'TECNICOS', 'abreviatura'=> 'TEC','activo'=>'1'
    ));

}
}