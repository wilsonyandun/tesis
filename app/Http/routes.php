<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::controller('/direccion','DireccionController');
Route::controller('/usuario','UsuarioController');
Route::controller('/escuela','EscuelaController');
Route::controller('/fuerza','FuerzaController');
Route::controller('/grado','GradoController');
Route::controller('/arma','ArmaController');
Route::controller('/persona','PersonaController');
Route::controller('/unidad','UnidadController');
Route::controller('/usuario','UsuarioController');
Route::controller('/curso','CursoController');
Route::controller('/reporte','ReporteController');
Route::controller('/perfil','ProfileController');