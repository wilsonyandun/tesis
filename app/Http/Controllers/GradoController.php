<?php namespace App\Http\Controllers;
use App\Fuerza;
use App\Grado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Auditoria;

class GradoController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('admin_direccion');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function getLista($msg=null)
	{
        $title='Grados | Lista ';
        $page_title='Lista de Grados';
        $nav1='Grados';
        $nav2='Lista';
        $grados=Grado::all();
		return view('grado.list',array('msg'=>$msg,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'grados'=>$grados
        ));
	}
    public function getNuevo(){
        $title='Grados | Crear ';
        $page_title='Creación de Grado';
        $nav1='Grados';
        $nav2='Crear';
        $action='GradoController@postNuevo';
        $grado=new Grado();
        $fuerzas=Fuerza::where('activo','1');
        $fuerzas=$fuerzas->lists('abreviatura','id');
        return view('grado.form',array('action'=>$action,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'grado'=>$grado,'fuerzas'=>$fuerzas
        ));
    }
    public function postNuevo(Request $request){
        $data=$request->all();
        $fuerzas=Fuerza::where('activo','1');
        $fuerzas=$fuerzas->lists('id');
        $fuerzas=implode(',',$fuerzas);
        $rules= array(
            'nombre'=>'required|unique:fuerzas',
            'abreviatura'=>'required',
            'fuerza_id'=>'required|numeric|in:'.$fuerzas);
        $v=Validator::make($data,$rules);
        if($v->fails()){
           return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
            DB::enableQueryLog();
        $grado= Grado::create($request->all());
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"CREAR GRADO","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
        return redirect()->back()->with('msg','creado');
        }
    }
    public function getEditar($id){
        $title='Grados | Editar ';
        $page_title='Edición de Grado';
        $nav1='Grados';
        $nav2='Crear';
        $action='GradoController@postEditar';
        $grado=Grado::find($id);
        $fuerzas=Fuerza::where('activo','1');
        $fuerzas=$fuerzas->lists('abreviatura','id');
        return view('grado.form',array('action'=>$action,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'grado'=>$grado,'fuerzas'=>$fuerzas
        ));
    }
    public function postEditar(Request $request){
        $data=$request->all();
        $rules= array(
            'nombre'=>'required',
            'abreviatura'=>'required');
        $v=Validator::make($data,$rules);
        if($v->fails()){
            return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
            $grado=Grado::find($request['grado_id']);
            DB::enableQueryLog();
            $grado->update($request->all());
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"MODIFICAR GRADO","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
        return redirect()->back()->with('msg','modificado');
        }
    }
    public function postEliminar(Request $request){
        $grado=Grado::find($request['id']);
        $data=array("activo"=>"0");
        DB::enableQueryLog();
        $grado->update($data);
        $queries = DB::getQueryLog();
        $last_query = end($queries);
        $last_query=implode(" , ", array_flatten($last_query));
        DB::disableQueryLog();
        $info=array("ip"=>$request->getClientIp(),"accion"=>"ELIMINAR GRADO","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
        $auditoria=Auditoria::create($info);
        $validate=true;
        echo json_encode(array('validate'=>$validate));
    }


}
