<?php namespace App\Http\Controllers;
use App\Auditoria;
use Illuminate\Support\Facades\URL;
use App\Curso;
use App\CursoCapacitacion;
use App\CursoEducacionRegular;
use App\CursoEspecializacion;
use App\CursoFormacion;
use App\CursoPerfeccionamiento;
use App\CursoPersona;
use App\Escuela;
use App\Nacionalidad;
use App\Persona;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Faker\Provider\fr_BE\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CursoController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin_escuela');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function getLista($msg = null)
    {
        $title = 'Cursos | Lista ';
        $page_title = 'Lista de Cursos';
        $nav1 = 'Cursos';
        $nav2 = 'Lista';
        $cursos=new Collection();
        if(Auth::user()->tipo=='0'){
        $cursos = Curso::all();
        }elseif(Auth::user()->tipo=='1'){
            $direccion=Auth::user()->escuela->direccion()->get();
            $escuelas=Escuela::where('direccion_id','=',$direccion[0]->id)->get();
            foreach($escuelas as $escuela){
            $cursos=$cursos->merge(Curso::where("escuela_id", '=', $escuela->id)->get());
            }
        }elseif(Auth::user()->tipo=='2'){
            $cursos=$cursos->merge(Curso::where("escuela_id", '=',Auth::user()->escuela_id)->get());
        }

        return view('curso.list', array('msg' => $msg, 'i' => '0', 'title' => $title, 'page_title' => $page_title, 'nav1' => $nav1, 'nav2' => $nav2
        , 'cursos' => $cursos
        ));
    }

    public function getNuevo()
    {
        $title = 'Cursos | Crear ';
        $page_title = 'Crear de Curso';
        $nav1 = 'Cursos';
        $nav2 = 'Crear';
        $action = 'CursoController@postNuevo';
        $curso = new Curso();
        $now = Carbon::now();
        $curso->fecha_inicio=$now->toDateString();
        $curso->fecha_fin=$now->toDateString();
        $paises = Nacionalidad::all();
        $paises = $paises->lists('nombre', 'id');
        if(Auth::user()->tipo=='0'){
        $escuelas = Escuela::all();
        }elseif(Auth::user()->tipo=='1'){
            $direccion=Auth::user()->escuela->direccion()->get();
            $escuelas=Escuela::where('direccion_id','=',$direccion[0]->id)->get();
        }elseif(Auth::user()->tipo=='2'){
            $escuelas=Escuela::where("id","=",Auth::user()->escuela_id)->get();
        }
        $escuelas = $escuelas->lists('nombre', 'id');
        $tipos = array('1' => 'CAPACITACIÓN', '2' => 'ESPECIALIZACIÓN', '3' => 'FORMACIÓN', '4' => 'PERFECCIONAMIENTO', '5' => 'EDUCACIÓN REGULAR');
        return view('curso.form', array('tipos' => $tipos, 'escuelas' => $escuelas, 'paises' => $paises, 'action' => $action, 'i' => '0', 'title' => $title, 'page_title' => $page_title, 'nav1' => $nav1, 'nav2' => $nav2
        , 'curso' => $curso
        ));
    }

    public function postNuevo(Request $request)
    {
        $data = $request->all();
        $rules = array(
            'nombre' => 'required',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required',
            'costo' => 'required',
            'ciudad' => 'required',
            'pais_id' => 'required');
        $v = Validator::make($data, $rules);
        if ($v->fails()) {
            return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        } else {
            DB::enableQueryLog();
            $curso = Curso::create($data);
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"CREAR CURSO","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
            $data['curso_id']=$curso->id;

            $tipo=$data['tipo'];
            if ($tipo == 1)
                $curso_nuevo = CursoCapacitacion::create($data);
            if ($tipo == 2)
                $curso_nuevo = CursoEspecializacion::create($data);
            if ($tipo == 3)
                $curso_nuevo = CursoFormacion::create($data);
            if ($tipo == 4)
                $curso_nuevo = CursoPerfeccionamiento::create($data);
            if ($tipo == 5)
                $curso_nuevo = CursoEducacionRegular::create($data);

            return redirect()->back()->with('msg', 'creado');
        }
    }

    public function getEditar($id)
    {
        $title = 'Cursos | Editar ';
        $page_title = 'Edición de Curso';
        $nav1 = 'Cursos';
        $nav2 = 'Crear';
        $action = 'CursoController@postEditar';
        $curso = Curso::find($id);
        $paises = Nacionalidad::all();
        $paises = $paises->lists('nombre', 'id');
        if(Auth::user()->tipo=='0'){
            $escuelas = Escuela::all();
        }elseif(Auth::user()->tipo=='1'){
            $direccion=Auth::user()->escuela->direccion()->get();
            $escuelas=Escuela::where('direccion_id','=',$direccion[0]->id)->get();
        }elseif(Auth::user()->tipo=='2'){
            $escuelas=Escuela::where("id","=",Auth::user()->escuela_id)->get();
        }
        $escuelas = $escuelas->lists('nombre', 'id');
        $tipos = array('1' => 'CAPACITACIÓN', '2' => 'ESPECIALIZACIÓN', '3' => 'FORMACIÓN', '4' => 'PERFECCIONAMIENTO', '5' => 'EDUCACIÓN REGULAR');
        return view('curso.form', array('tipos' => $tipos, 'escuelas' => $escuelas, 'paises' => $paises, 'action' => $action, 'i' => '0', 'title' => $title, 'page_title' => $page_title, 'nav1' => $nav1, 'nav2' => $nav2
        , 'curso' => $curso
        ));
    }

    public function postEditar(Request $request)
    {
        $data = $request->all();
        $rules = array(
            'nombre' => 'required|unique:cursos,nombre,' . $request['curso_id'],
            'abreviatura' => 'required');
        $v = Validator::make($data, $rules);
        if ($v->fails()) {
            return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        } else {
            $curso = Curso::find($request['curso_id']);
            DB::enableQueryLog();
            $curso->update($request->all());
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"MODIFICAR CURSO","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
            return redirect()->back()->with('msg', 'modificado');
        }
    }

    public function postEliminar(Request $request)
    {
        $curso = Curso::find($request['id']);
        $data = array("activo" => "0");
        DB::enableQueryLog();
        $curso->update($data);
        $queries = DB::getQueryLog();
        $last_query = end($queries);
        $last_query=implode(" , ", array_flatten($last_query));
        DB::disableQueryLog();
        $info=array("ip"=>$request->getClientIp(),"accion"=>"ELIMINAR CURSO","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
        $auditoria=Auditoria::create($info);
        $validate = true;
        echo json_encode(array('validate' => $validate));
    }
    public function postEliminarm(Request $request)
    {
        $data=$request->all();
        $curso = CursoPersona::where('persona_id','=',$data['persona_id'])->where('curso_id','=',$data['curso_id'])->first();
        DB::enableQueryLog();
        $curso_persona=CursoPersona::find($curso->id);
        $curso_persona->delete();
        $queries = DB::getQueryLog();
        $last_query = end($queries);
        $last_query=implode(" , ", array_flatten($last_query));
        DB::disableQueryLog();
        $info=array("ip"=>$request->getClientIp(),"accion"=>"ELIMINAR MATRICULA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
        $auditoria=Auditoria::create($info);
        $validate = true;
        echo json_encode(array('validate' => $validate));
    }

    public function getMatricular()
    {
        $title = 'Cursos Activos | Lista ';
        $page_title = 'Lista de Cursos para Matrículas';
        $nav1 = 'Cursos';
        $nav2 = 'Lista Matriculas';
        $cursos=new Collection();
        if(Auth::user()->tipo=='0'){
            $cursos = Curso::where('activo', '=', '1')->get();;
        }elseif(Auth::user()->tipo=='1'){
            $direccion=Auth::user()->escuela->direccion()->get();
            $escuelas=Escuela::where('direccion_id','=',$direccion[0]->id)->get();
            foreach($escuelas as $escuela){
                $cursos=$cursos->merge(Curso::where("escuela_id", '=', $escuela->id)->where('activo', '=', '1')->get());
            }
        }elseif(Auth::user()->tipo=='2'){
            $cursos=$cursos->merge(Curso::where("escuela_id", '=',Auth::user()->escuela_id)->where('activo', '=', '1')->get());
        }
        return view('curso.matricula', array('i' => '0', 'title' => $title, 'page_title' => $page_title, 'nav1' => $nav1, 'nav2' => $nav2
        , 'cursos' => $cursos
        ));
    }
    public function getFinalizar($msg=null)
    {
        $title = 'Cursos  | Finalización ';
        $page_title = 'Finalización de Cursos';
        $nav1 = 'Cursos';
        $nav2 = 'Finalización';
        $cursos=new Collection();
        if(Auth::user()->tipo=='0'){
            $cursos = Curso::where('activo', '=', '1')->get();;
        }elseif(Auth::user()->tipo=='1'){
            $direccion=Auth::user()->escuela->direccion()->get();
            $escuelas=Escuela::where('direccion_id','=',$direccion[0]->id)->get();
            foreach($escuelas as $escuela){
                $cursos=$cursos->merge(Curso::where("escuela_id", '=', $escuela->id)->where('activo', '=', '1')->get());
            }
        }elseif(Auth::user()->tipo=='2'){
            $cursos=$cursos->merge(Curso::where("escuela_id", '=',Auth::user()->escuela_id)->where('activo', '=', '1')->get());
        }
        return view('curso.finalizar', array('msg'=>$msg,'i' => '0', 'title' => $title, 'page_title' => $page_title, 'nav1' => $nav1, 'nav2' => $nav2
        , 'cursos' => $cursos
        ));
    }

    public function getMatricula($id,$msg=null)
    {
        if($msg=="ok"){
            $msg="Matrícula eliminada correctamente";
        }
        $title = 'Cursos | Matrículas ';
        $page_title = 'Matrículas de Curso';
        $nav1 = 'Cursos';
        $nav2 = 'Matrículas';
        $curso = Curso::find($id);

        $persona = new Persona();
        return view('curso.matricula_list', array('msg'=>$msg,'persona' => $persona, 'i' => '0', 'title' => $title, 'page_title' => $page_title, 'nav1' => $nav1, 'nav2' => $nav2
        , 'curso' => $curso
        ));
    }
    public function getFinaliza($id)
    {
        $title = 'Cursos | Finalización ';
        $page_title = 'Finalización de Curso';
        $nav1 = 'Cursos';
        $nav2 = 'Finalización';
        $curso = Curso::find($id);
        $persona = new Persona();
        $action = 'CursoController@postFinaliza';
        $estados = array('A'=>'APROBADO','R'=>'REPROBADO','O'=>'OTROS');
        return view('curso.finalizar_list', array('estados'=>$estados,'action'=>$action,'persona' => $persona, 'i' => '0', 'title' => $title, 'page_title' => $page_title, 'nav1' => $nav1, 'nav2' => $nav2
        , 'curso' => $curso
        ));
    }
    public function postFinaliza(Request $request){
        $data=$request->all();
        $curso_id=$data['curso_id'];
        $personas=$data['persona_id'];
        $estados=$data['estados'];
        $observaciones=$data['observaciones'];
        $i=0;
        foreach($personas as $persona){
            $curso = CursoPersona::where('persona_id','=',$persona)->where('curso_id','=',$curso_id)->first();
            $curso_persona=CursoPersona::find($curso->id);
            $curso_persona->estado=$estados[$i];
            $curso_persona->observaciones=$observaciones[$i];
            $curso_persona->update();
            $i++;
        }
        $msg="ok";
        $curso=Curso::find($curso_id);
        $curso->activo='2';
        DB::enableQueryLog();
        $curso->update();
        $queries = DB::getQueryLog();
        $last_query = end($queries);
        $last_query=implode(" , ", array_flatten($last_query));
        DB::disableQueryLog();
        $info=array("ip"=>$request->getClientIp(),"accion"=>"FINALIZAR CURSO","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
        $auditoria=Auditoria::create($info);
        $title = 'Cursos  | Finalización ';
        $page_title = 'Finalización de Cursos';
        $nav1 = 'Cursos';
        $nav2 = 'Finalización';
        $cursos = Curso::where('activo', '=', '1')->get();
        return view('curso.finalizar', array('msg'=>$msg,'i' => '0', 'title' => $title, 'page_title' => $page_title, 'nav1' => $nav1, 'nav2' => $nav2
        , 'cursos' => $cursos
        ));
    }
    public function getMatriculado($curso,$persona){
        $data=array('curso_id'=>$curso,'persona_id'=>$persona,'estado'=>'P');
        $matricula=CursoPersona::where('curso_id','=',$curso)->where('persona_id','=',$persona)->get();
        if(count($matricula)>0){
            $msg="El alumno ya está matriculado en este curso";
        }else{
            DB::enableQueryLog();
            $matricula=CursoPersona::create($data);
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$_SERVER['REMOTE_ADDR'],"accion"=>"MATRICULA ID=".$persona,"sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
            $msg="Alumno matriculado correctamente";
        }
        $title = 'Cursos | Matrículas ';
        $page_title = 'Matrículas de Curso';
        $nav1 = 'Cursos';
        $nav2 = 'Matrículas';
        $curso = Curso::find($curso);
        $persona = new Persona();
        return view('curso.matricula_list', array('msg'=>$msg,'persona' => $persona, 'i' => '0', 'title' => $title, 'page_title' => $page_title, 'nav1' => $nav1, 'nav2' => $nav2
        , 'curso' => $curso
        ));
    }

    public function postTabla(Request $request)
    {
        $i = 1;
        $data = $request->all();
        $personas = Persona::where('nombres', 'like', '%' . $data['nombres'] . '%')
            ->where('apellidos', 'like', '%' . $data['apellidos'] . '%')
            ->where('identificacion', 'like', '%' . $data['identificacion'] . '%')->get();
        $id=$data['curso_id'];
        $html = '<div class="table-responsive">
                                        <table id="table_id"
                                               class="table table-hover table-striped table-bordered table-advanced tablesorter display">
                                            <thead>
                                            <tr>

                                                <th width="9%">#</th>
                                                <th width="10%">Nombres</th>
                                                <th width="10%">Apellidos</th>
                                                <th width="10%">Identificación</th>
                                                <th width="10%">Grado</th>
                                                <th width="12%">Acciones</th>
                                            </tr>
                                            <tbody>';

        foreach ($personas as $persona) {

            $html .= '
                                            <tr>
                                                <td>' . $i++ . '</td>
                                                <td>' . $persona->nombres . '</td>
                                                <td>' . $persona->apellidos . '</td>
                                                <td>' . $persona->identificacion . '</td>';
            if (count($persona->grado_personas) > 0)
                $html .= '<td>' . $persona->grado_personas->last()->grado->nombre . '</td>';
            else
                $html .= '<td></td>';
            $html .= '
                                                <td>
                                                    <a href="'.URL::to('/').'/curso/matriculado/'.$id.'/'. $persona->id . '" type="button" class="btn btn-default btn-xs mbs"><i class="fa fa-check"></i>&nbsp;
                                                        Matricular
                                                    </a>
                                                </td>';

            $html .= '
                                            </tr>';
        };
        $html .= "</tbody>
                                            </thead></table>
                                    </div>";

        echo json_encode(array('tabla' => $html));
    }


}
