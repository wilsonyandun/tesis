@extends('layout.master')
@section('aditional_css')
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/dataTables.bootstrap.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-colorpicker/css/colorpicker.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-datepicker/css/datepicker.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-daterangepicker/daterangepicker-bs3.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-clockface/css/clockface.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-switch/css/bootstrap-switch.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/select2/select2-madmin.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-select/bootstrap-select.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/multi-select/css/multi-select-madmin.css">
@stop
@section('content')
<div class="col-lg-12">
<div class="portlet box">
<div class="portlet-header">
    <div class="caption">Reporte General</div>
</div>
<div class="portlet-body">
    {!! Form::model($data,['action'=>$action,'method'=>'POST','id'=>'form_data','class'=>'form-horizontal form-bordered form-validate']) !!}
    <div class="form-body">

        <div class="form-group">
            <label for="fecha_inicio"
                   class="col-md-2 control-label">
                Fecha </label>

            <div class="col-md-10">
                <div class="btn btn-blue reportrange"><i class="fa fa-calendar"></i>&nbsp;<span></span>&nbsp;<i
                        class="fa fa-angle-down"></i><input type="hidden"
                                                            name="datestart" value="{{$data['datestart']}}"/><input
                        type="hidden" name="endstart" value="{{$data['endstart']}}"/></div>
            </div>
            <label for="direccion_id"
                   class="col-md-2 control-label">
                Dirección </label>

            <div class="col-md-4 col-sm-4 col-xs-4">{!!Form::select('direccion_id', $direcciones,$data['direccion_id'],array("id"=>"direcciones","class"=>"select2-size form-control")) !!}</div>
            <label for="escuela_id"
                   class="col-md-2 control-label">
                Escuela</label>

            <div class="col-md-4 col-sm-4 col-xs-4">{!!Form::select('escuela_id', $escuelas, $data['escuela_id'],array("id"=>"escuelas","class"=>"select2-size form-control")) !!}</div>

            <label for="tipo"
                   class="col-md-2 control-label">
                Tipo de Curso</label>

            <div class="col-md-4 col-sm-4 col-xs-4">{!!Form::select('tipos', $tipos, $data['tipos'],array("id"=>"tipos","class"=>"select2-size form-control")) !!}</div>

            <label for="curso_id"
                   class="col-md-2 control-label">
                Nombre de Curso</label>

            <div class="col-md-4 col-sm-4 col-xs-4">{!!Form::select('curso_id', $cursos, $data['curso_id'],array("id"=>"cursos","class"=>"select2-size form-control")) !!}</div>

            <label for="identificacion"
                   class="col-md-2 control-label">
                Atributo</label>


                <div class="col-md-4 col-sm-4 col-xs-4">{!!Form::select('atributos', $atributos, $data['atributos'],array("id"=>"atributos","class"=>"select2-size form-control")) !!}</div>
            <label for="identificacion"
                   class="col-md-2 control-label">
                Valor</label>

            <div class="col-md-4 col-sm-4 col-xs-4">{!!Form::select('valores', $valores, $data['valores'],array("id"=>"valores","class"=>"select2-size form-control")) !!}</div>
            <div class="col-md-2 control-label"></div>
            <div class="col-md-4 ">
                <button id="enviar" type="submit" class="btn btn-primary col-md-4">Enviar</button>
                &nbsp;
            </div>

        </div>
        </div>
        {!! Form::close() !!}
@if(isset($cursos_reporte))
    @if(count($cursos_reporte)>0)
    <div class="portlet box">
        <div class="portlet-header">
            <div class="caption">Scatter plot</div>
            <div class="tools"><i class="fa fa-chevron-up"></i><i data-toggle="modal"
                                                                  data-target="#modal-config"
                                                                  class="fa fa-cog"></i><i
                    class="fa fa-refresh"></i><i class="fa fa-times"></i></div>
        </div>
        <div class="portlet-body">
            @if(isset($series))
            <div id="column-line-and-pie"></div>
            @endif
        </div>
    </div>
    @endif
<div class="row mbm">
<div class="col-lg-12">
<div class="table-responsive">
<table id="table_id"
       class="table table-hover table-striped table-bordered table-advanced tablesorter display">
<thead>
<tr>

    <th width="5%">#</th>
    <th >Nombre</th>
    <th >Fecha Finalización</th>
    <th>Tipo</th>
    @if($data['atributos']==0)
    <th width="10%"># Personas</th>
        @elseif($data['atributos']==1)
            @if($data['valores']=='0')
             <th>Hombres</th><th>Mujeres</th>
            @elseif($data['valores']=='H')
            <th>Hombres</th>
             @else
            <th>Mujeres</th>
            @endif
        @elseif($data['atributos']==2)
            @if($data['valores']=='0')
             <th>Nacional</th><th>Extranjero</th>
             @elseif($data['valores']=='N')
             <th>Nacional</th>
            @else
            <th>Extranjero</th>
            @endif
        @elseif($data['atributos']==3)
            @if($data['valores']=='0')
            <th>Aprobado</th><th>Reprobado</th><th>Otros</th>
            @elseif($data['valores']=='A')
            <th>Aprobado</th>
            @elseif($data['valores']=='R')
            <th>Reprobado</th>
            @else
            <th>Otros</th>
        @endif
    @endif
    <th>Acciones</th>

</tr>
<tbody>
@foreach($cursos_reporte as $selec)
<tr>
    <td>{{++$i}}</td>
    <td>{{$selec->nombre}}</td>
    <td>{{$selec->fecha_fin}}</td>
    <td>Tipo</td>
    @if(isset($val1)&& $val1!=""&&$val2==""&&$val3==""&&$data['atributos']=='0')
    <td>{{count($selec->curso_persona)}}</td>
    @endif

    <!--GENERO -->
    @if($data['atributos']==1&&$data['valores']=='0')
    <td>{{count($selec->alumnos()->hombres()->get())}}</td>
    <td>{{count($selec->alumnos()->mujeres()->get())}}</td>
    @endif
    @if($data['atributos']=='1'&&$data['valores']=='H')
    <td>{{count($selec->alumnos()->hombres()->get())}}</td>
    @endif
    @if($data['atributos']=='1'&&$data['valores']=='M')
    <td>{{count($selec->alumnos()->mujeres()->get())}}</td>
    @endif
    <!--EXTRANJEROS -->

    @if($data['atributos']=='2'&&$data['valores']=='0')
    <td>{{count($selec->alumnos()->nacionales()->get())}}</td>
    <td>{{count($selec->alumnos()->get())-count($selec->alumnos()->nacionales()->get())}}</td>
    @endif
    @if($data['atributos']=='2'&&$data['valores']=='N')
    <td>{{count($selec->alumnos()->nacionales()->get())}}</td>
    @endif
    @if($data['atributos']=='2'&&$data['valores']=='E')
    <td>{{count($selec->alumnos()->get())-count($selec->alumnos()->nacionales()->get())}}</td>
    @endif
    <!-- CALIFACIONES -->

    @if($data['atributos']==3&&$data['valores']=='0')
    <td>{{count($selec->curso_persona()->aprobados()->get())}}</td>
    <td>{{count($selec->curso_persona()->reprobados()->get())}}</td>
    <td>{{count($selec->curso_persona()->otros()->get())}}</td>
    @endif
    @if($data['atributos']==3&&$data['valores']=='A')
    <td>{{count($selec->curso_persona()->aprobados()->get())}}</td>
    @endif
    @if($data['atributos']==3&&$data['valores']=='R')
    <td>{{count($selec->curso_persona()->reprobados()->get())}}</td>
    @endif
    @if($data['atributos']==3&&$data['valores']=='O')
    <td>{{count($selec->curso_persona()->otros()->get())}}</td>
    @endif

    <td>
        <a href="{{env('APP_BASE')}}reporte/curso-lista/{{$selec->id}}" type="button" class="btn btn-default btn-xs mbs"><i class="fa fa-search"></i>&nbsp;
            Ver Listado
        </a>
    </td>

</tr>
@endforeach

</tbody>
</thead></table>
</div>
</div>
</div>

@endif
    @if(isset($cursos_reporte))
@if($data['atributos']==0)
<h4>Total: {{$val1}} personas</h4>
@elseif($data['atributos']==1)
    @if($data['valores']=='0')
    <h4>Total: {{$val1}} hombres {{$val2}} mujeres</h4>
    @elseif($data['valores']=='H')
    <h4>Total: {{$val1}} hombres </h4>
    @else
    <h4>Total: {{$val1}} mujeres </h4>
    @endif
@elseif($data['atributos']==2)
    @if($data['valores']=='0')
    <h4>Total: {{$val1}} nacionales {{$val2}} extranjeros</h4>
    @elseif($data['valores']=='N')
    <h4>Total: {{$val1}} nacionales </h4>
    @else
    <h4>Total: {{$val1}} extranjeros </h4>
    @endif
@elseif($data['atributos']==3)
    @if($data['valores']=='0')
    <h4>Total: {{$val1}} aprobados {{$val2}} reprobados {{$val3}} otros</h4>
    @elseif($data['valores']=='A')
    <h4>Total: {{$val1}} aprobados </h4>
    @elseif($data['valores']=='R')
    <h4>Total: {{$val1}} reprobados </h4>
    @else
    <h4>Total: {{$val1}} otros </h4>
    @endif
@endif
@endif
</div>

</div>
</div>


@stop

@section('aditional_scripts')
@if(isset($cursos_reporte))
<script src="{{env('APP_BASE')}}vendors/DataTables/media/js/jquery.dataTables.js"></script>
<script src="{{env('APP_BASE')}}vendors/DataTables/media/js/dataTables.bootstrap.js"></script>
<script src="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script src="{{env('APP_BASE')}}js/table-datatables.js"></script>
@endif
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/sco.message/sco.message.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/notific8.js"></script>
<script src="{{env('APP_BASE')}}js/ui-notific8.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-validate/jquery.validate.min.js"></script>
<script src="{{env('APP_BASE')}}js/form-validation.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="{{env('APP_BASE')}}vendors/moment/moment.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-clockface/js/clockface.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-maskedinput/jquery-maskedinput.js"></script>
<script src="{{env('APP_BASE')}}vendors/charCount.js"></script>
<script src="{{env('APP_BASE')}}js/form-components.js"></script>
<script src="{{env('APP_BASE')}}vendors/select2/select2.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/multi-select/js/jquery.multi-select.js"></script>
<script src="{{env('APP_BASE')}}js/ui-dropdown-select.js"></script>

<script src="{{env('APP_BASE')}}vendors/jquery-highcharts/exporting.js"></script>


<script>
    var fecha="{{$data['datestart']}}  -  {{$data['endstart']}}"
    $('.reportrange span').html(fecha);
    $('#direcciones').on('change', function(e) {
        var token ="{{csrf_token()}}";
            $("#escuelas").select2("val", 0);
            $("#cursos").select2("val", 0);
            $("#tipos").select2("val", 0);
        $.post("{{URL::to('/')}}/reporte/escuelas",{
                direccion_id: $('#direcciones').val(),
                _token:token
            },
            function(json){
                $('#escuelas').html(json.opciones);
                $('#cursos').html(json.cursos);
            }
            ,"json");

    });
    $('#escuelas').on('change', function(e) {
            $("#cursos").select2("val", 0);
            $("#tipos").select2("val", 0);
        var token ="{{csrf_token()}}";
        $.post("{{URL::to('/')}}/reporte/cursos",{
                escuela_id: $('#escuelas').val(),
                _token:token
            },
            function(json){
                $('#cursos').html(json.cursos);
            }
            ,"json");

    });
    $('#tipos').on('change', function(e) {
        $("#cursos").select2("val", 0);
        var token ="{{csrf_token()}}";
        $.post("{{URL::to('/')}}/reporte/tipos",{
                tipo: $('#tipos').val(),
                direccion_id: $('#direcciones').val(),
                escuela_id: $('#escuelas').val(),
                _token:token
            },
            function(json){
                $('#cursos').html(json.cursos);
            }
            ,"json");

    });
    $('#atributos').on('change', function(e) {
        $("#valores").select2("val", 0);
        var token ="{{csrf_token()}}";
        $.post("{{URL::to('/')}}/reporte/atributos",{
                atributo: $('#atributos').val(),
                _token:token
            },
            function(json){
                $('#valores').html(json.valores);
            }
            ,"json");

    });
    @if(isset($series))
    $(function () {

        /* Column, line and pie */
        $('#column-line-and-pie').highcharts({
            chart: {

            },
            title: {
                text: 'Reporte Gráfico'
            },
            xAxis: {
                categories: ['Personas']
            },
            tooltip: {
                formatter: function() {
                    var s;
                    if (this.point.name) { // the pie chart
                        s = ''+
                            this.point.name +': '+ this.y ;
                    } else {
                        s = ''+
                            this.x  +': '+ this.y;
                    }
                    return s;
                }
            },
            labels: {
                items: [{
                    html: 'Total personas',
                    style: {
                        left: '40px',
                        top: '8px',
                        color: 'black'
                    }
                }]
            },
            series: [
                @foreach($series as $serie)
                 {
            type: 'column',
            name: "{{$serie['nombre']}}",
            data: [{{$serie["data"]}}]
            },
            @endforeach
                 {
                type: 'pie',
                name: 'Total Personas',
                data: [
                 @foreach($series as $serie)
                        {
                     name: "{{$serie['nombre']}}",
                       y: {{$serie['data']}},
                    // color: Highcharts.getOptions().colors[4] // Jane's color
    },
                @endforeach
                ],
                center: [40, 80],
                size: 120,
                showInLegend: false,
                dataLabels: {
                    enabled: false
                }
            }]
        });


    });
    @endif
</script>


@stop