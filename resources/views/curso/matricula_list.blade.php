@extends('layout.master')
@section('aditional_css')
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet"
      href="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/dataTables.bootstrap.css">
@stop
@section('content')
<div class="col-lg-12">
<div class="portlet box">
<div class="portlet-header">
    <div class="caption">Lista de Alumnos Matriculados</div>
</div>
<div class="portlet-body">
    @if(isset($msg))
    <div class="alert alert-success alert-dismissable">
        <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
        <h4 style="color: #000000">{{$msg}}</h4>
    </div>
    @endif
    <div class="row mbm">
        <div class="col-lg-12">
<h2>Nombre del Curso: {{$curso->nombre}}</h2>
<br>
    <div class="form-group">

        <div class="col-md-4">
            <button id="modal_matricula"type="button" data-target="#modal-wide-width"
                    data-toggle="modal" class="btn btn-blue">Matricular Alumno
            </button>
        </div>
    </div>
</div>
        </div>
<div class="row mbm">
<div class="col-lg-12">
<div class="table-responsive">
<table id="table_id"
       class="table table-hover table-striped table-bordered table-advanced tablesorter display">
    <thead>
    <tr>

        <th width="9%">#</th>
        <th width="10%">Nombres</th>
        <th width="10%">Apellidos</th>
        <th width="10%">Identificación</th>
        <th width="10%">Estado</th>
        <th width="12%">Acciones</th>
    </tr>
    <tbody>
    @foreach($curso->alumnos as $persona)
    <tr>
        <td>{{++$i}}</td>
        <td>{{$persona->nombres}}</td>
        <td>{{$persona->apellidos}}</td>
        <td>{{$persona->identificacion}}</td>
        @if($persona->activo==1)
        <td><span class="label label-sm label-success">Activo</span></td>
        <td>
            <button data-id="{{$persona->id}}" data-toggle="modal" type="button" class="confirm-delete btn btn-danger btn-xs mbs"><i class="fa fa-trash-o"></i>&nbsp;
                Eliminar
            </button>
        </td>
        @else
        <td><span class="label label-sm label-warning">Inactivo</span></td>
        <td></td>
        @endif

    </tr>
    @endforeach

    </tbody>
    </thead></table>
</div>
</div>
</div>
</div>
</div>
</div>
<div id="modal-wide-width" tabindex="-1" role="dialog" aria-labelledby="modal-wide-width-label"
     aria-hidden="true" class="modal fade">
    <div class="modal-dialog modal-wide-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                        class="close">&times;</button>
                <h4 id="modal-wide-width-label" class="modal-title">Modal Wide Width</h4></div>
            <div class="modal-body">

                            <div class="form-horizontal form-bordered" >
                            <div class="form-body">

                                <div class="form-group">

                                    <label for="nombres"
                                           class="col-md-2 control-label">
                                        Nombres </label>

                                    <div class="col-md-4"><input name="nombres"  id="nombres" type="text"
                                                                 placeholder="" value="{{$persona->nombres}}"
                                                                 class="form-control  "/></div>
                                    <label for="apellidos"
                                           class="col-md-2 control-label">
                                        Apellidos</label>

                                    <div class="col-md-4"><input name="apellidos" id="apellidos" type="text"
                                                                 placeholder=""
                                                                 value="{{$persona->apellidos}}"
                                                                 class="form-control  "/></div>

                                    <label for="identificacion"
                                           class="col-md-2 control-label">
                                        Identificación</label>

                                    <div class="col-md-4"><input name="identificacion" id="identificacion" type="text"
                                                                 placeholder=""
                                                                 value="{{$persona->identificacion}}"
                                                                 class="form-control  "/></div>
                                    <div class="col-md-2 control-label"></div>
                                    <div class="col-md-4 ">
                                        <button id="enviar_info"  class="btn btn-primary col-md-4">Guardar</button>
                                        &nbsp;
                                    </div>

                                </div>
                            </div>
                            </div>

                            <div class="row mbm">
                                <div class="col-lg-12" id="tabla_alumno">

                                </div>
                            </div>





            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="modal-confirm" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">Are you sure?</div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button id="btnYes" type="button" data-dismiss="modal" class="btn btn-primary">Ok</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('aditional_scripts')
<script src="{{env('APP_BASE')}}vendors/DataTables/media/js/jquery.dataTables.js"></script>
<script src="{{env('APP_BASE')}}vendors/DataTables/media/js/dataTables.bootstrap.js"></script>
<script src="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script src="{{env('APP_BASE')}}js/table-datatables.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/sco.message/sco.message.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/notific8.js"></script>
<script src="{{env('APP_BASE')}}js/ui-notific8.js"></script>
<script>
    $('#enviar_info').on('click', function(e) {
            var token ="{{csrf_token()}}";
        $.post("{{URL::to('/')}}/curso/tabla",{
            nombres: $("#nombres").val(),
            apellidos: $("#apellidos").val(),
            identificacion: $("#identificacion").val(),
            curso_id: {{$curso->id}},
            _token:token
    },
        function(json){
        $("#tabla_alumno").empty();
        $("#tabla_alumno").html(json.tabla);
        }
        ,"json");
    });
    $('#modal_matricula').on('click', function(e) {
    $('#apellidos').val("");
    $('#nombres').val("");
    $('#identificacion').val("");
    });
    $('.confirm-delete').on('click', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        $('#modal-confirm').data('id', id).modal('show');
    });
    $('#btnYes').click(function() {
        // handle deletion here
        var persona_id = $('#modal-confirm').data('id');
        var token ="{{csrf_token()}}";
        var curso_id= "{{$curso->id}}";
        $.post("{{URL::to('/')}}/curso/eliminarm",{
         curso_id:curso_id,
        persona_id: persona_id,
        _token:token },
            function(json){
                if(json.validate==true){
                    window.location="{{URL::to('/')}}/curso/matricula/"+curso_id+"/ok";
                }

            }
            ,"json");
        $('#modal-confirm').modal('hide');
    });

</script>

@stop