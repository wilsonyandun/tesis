<!DOCTYPE html>
<html lang="en">
<head><title>Login</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="Thu, 19 Nov 1900 08:52:00 GMT">
    <!--Loading bootstrap css-->
    <link type="text/css"
          href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,800italic,400,700,800">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link type="text/css" rel="stylesheet"
          href="{{env('APP_BASE')}}vendors/jquery-ui-1.10.4.custom/css/ui-lightness/jquery-ui-1.10.4.custom.css">
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/font-awesome/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/pageloader/pageloader.css">
    <!--Loading style vendors-->
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/animate.css/animate.css">
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/iCheck/skins/all.css">
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/jquery-pace/pace.css">
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.css">
    <!--Loading style-->
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}css/themes/style1/pink-blue.css" class="default-style">
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}css/themes/style1/pink-blue.css" id="theme-change"
          class="style-change color-change">

    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}css/style-responsive.css">
    <link rel="shortcut icon" href="{{env('APP_BASE')}}images/favicon.ico">
</head>
<body id="signin-page" style="background: url('{{env('APP_BASE')}}images/fondo.png') center center fixed; width: 100%;height: 100%">
<div id="imgLOAD" style="text-align: center">
    <b>Cargando</b>
    <img alt="LOADING" height='50' src="{{env('APP_BASE')}}images/fondo.png" width="50"/>
</div>
<div class="page-form">

    <form action="{{env('APP_BASE')}}auth/login"  method="post" class="form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="header-content"><h1>Entrar</h1></div>
        <div class="body-content">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                 Hay problemas con la información que ingresaste.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="form-group">
                <div class="input-icon right"><i class="fa fa-user"></i><input type="text" placeholder="Username"
                                                                               name="username" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="input-icon right"><i class="fa fa-key"></i><input type="password" placeholder="Password"
                                                                              name="password" class="form-control">
                </div>
            </div>
            <div class="form-group pull-left">
                <div class="checkbox-list"><label><input type="checkbox">&nbsp;
                        Recordarme</label></div>
            </div>
            <div class="form-group pull-right">
                <button type="submit" class="btn btn-success">Entrar
                    &nbsp;<i class="fa fa-chevron-circle-right"></i></button>
            </div>
            <div class="clearfix"></div>

            <hr>
</div>

    </form>

</div>
<script src="{{env('APP_BASE')}}js/jquery-1.10.2.min.js"></script>
<script src="{{env('APP_BASE')}}js/jquery-migrate-1.2.1.min.js"></script>
<script src="{{env('APP_BASE')}}js/jquery-ui.js"></script>
<!--loading bootstrap js-->
<script src="{{env('APP_BASE')}}vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js"></script>
<script src="{{env('APP_BASE')}}js/html5shiv.js"></script>
<script src="{{env('APP_BASE')}}js/respond.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/iCheck/icheck.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/iCheck/custom.min.js"></script>
<script>//BEGIN CHECKBOX & RADIO
    $('input[type="checkbox"]').iCheck({
        checkboxClass: 'icheckbox_minimal-grey',
        increaseArea: '20%' // optional
    });
    $('input[type="radio"]').iCheck({
        radioClass: 'iradio_minimal-grey',
        increaseArea: '20%' // optional
    });
    //END CHECKBOX & RADIO</script>
<script type="text/javascript">(function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-145464-12', 'auto');
    ga('send', 'pageview');</script>
<script type='text/javascript'>
    window.onload = detectarCarga;
    function detectarCarga(){
        document.getElementById("imgLOAD").style.display="none";
    }
</script>

</body>
</html>