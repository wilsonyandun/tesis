@extends('layout.master')
@section('aditional_css')
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/animate.css/animate.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/jquery-pace/pace.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/iCheck/skins/all.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-daterangepicker/daterangepicker-bs3.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/select2/select2-madmin.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-select/bootstrap-select.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/multi-select/css/multi-select-madmin.css">
@stop
@section('content')

<div class="col-lg-12">
<div class="panel panel-blue">
    <div class="panel-heading">{{$page_title}}</div>
    <div class="panel-body pan">

        @if(Session::get('msg'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
            <h4 style="color: #000000">El usuario a sido {{Session::get('msg')}} exitosamente</h4>
        </div>
        @endif
        {!! Form::model($grado,['action'=>$action,'method'=>'POST','class'=>'form-horizontal form-bordered form-validate']) !!}
        @if(isset($grado->id))
        <input name="grado_id" type="hidden" value="{{$grado->id}}">
        @endif
            <div class="form-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    Hay problemas con la información que ingresaste.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="form-group">

                    <label for="nombre"
                                               class="col-md-3 control-label">
                        Nombre <span class='require'>*</span></label>

                    <div class="col-md-4"><input name="nombre" minlength="2" id="nombre" type="text"
                                                 placeholder="" value="{{$grado->nombre}}"
                                                 class="form-control required "/><span
                            class="help-block">Ej: DIRECCIÓN DE EDUCACIÓN Y DOCTRINA MILITAR </span></div>
                </div>
                <div class="form-group"><label for="abreviatura"
                                               class="col-md-3 control-label">
                        Abreviatura <span class='require'>*</span></label>

                    <div class="col-md-4"><input name="abreviatura" id="abreviatura" type="text"
                                                 placeholder=""
                                                 value="{{$grado->abreviatura}}"
                                                 class="form-control required "/><span
                            class="help-block">Ej: DIEDMIL </span></div>
                </div>

                <div class="form-group"><label for="abreviatura"
                                               class="col-md-3 control-label">
                        DIRECCIÓN <span class='require'>*</span></label>

                    <div class="col-md-6 col-sm-6 col-xs-6">{!!Form::select('fuerza_id', $fuerzas, $grado->fuerza_id,array("class"=>"select2-size form-control")) !!}
                        <span
                            class="help-block">Ej:DIEDMIL</span></div>
                </div>

            </div>
            <div class="form-actions text-right pal">
                <button type="submit" class="btn btn-primary">Guardar</button>
                &nbsp;
                <button type="button" class="btn btn-green">Cancelar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
</div>

@stop
@section('aditional_scripts')
<script src="{{env('APP_BASE')}}vendors/jquery-validate/jquery.validate.min.js"></script>
<script src="{{env('APP_BASE')}}js/form-validation.js"></script>
<script src="{{env('APP_BASE')}}vendors/select2/select2.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/multi-select/js/jquery.multi-select.js"></script>
<script src="{{env('APP_BASE')}}js/ui-dropdown-select.js"></script>
@stop