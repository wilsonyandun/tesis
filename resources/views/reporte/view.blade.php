@extends('layout.master')
@section('aditional_css')
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/DataTables/media/css/dataTables.bootstrap.css">
@stop
@section('content')
<div class="col-lg-12">
<div class="portlet box">
<div class="portlet-header">
    <div class="caption">Información de Persona</div>
</div>
<div class="portlet-body">

<div class="row mbm">
<div class="col-lg-12">
    <form action="#" class="form-horizontal" >
        <div class="form-body pal"><h3>Datos Personales</h3>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"><label for="inputFirstName"
                                                   class="col-md-3 control-label">
                            Nombres:</label>

                        <div class="col-md-9"><p
                                class="form-control-static">{{$persona->nombres}}</p></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group"><label for="inputLastName"
                                                   class="col-md-3 control-label">Apellidos:</label>

                        <div class="col-md-9"><p
                                class="form-control-static">{{$persona->apellidos}}</p></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"><label for="inputEmail"
                                                   class="col-md-3 control-label">Email:</label>

                        <div class="col-md-9"><p
                                class="form-control-static"><a
                                    href="mailto:whisfat1935@jourrapide.com">whisfat1935@jourrapide.com</a>
                            </p></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group"><label for="selGender"
                                                   class="col-md-3 control-label">Gender:</label>

                        <div class="col-md-9"><p
                                class="form-control-static">Male</p></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"><label for="inputBirthday"
                                                   class="col-md-3 control-label">Birthday</label>

                        <div class="col-md-9"><p
                                class="form-control-static">20.02.1985</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group"><label for="inputPhone"
                                                   class="col-md-3 control-label">Phone</label>

                        <div class="col-md-9"><p
                                class="form-control-static">607-656-6310</p>
                        </div>
                    </div>
                </div>
            </div>
            <h3>Cursos Recibidos</h3>
            <div class="row mbm">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table id="table_id"
                               class="table table-hover table-striped table-bordered table-advanced tablesorter display">
                            <thead>
                            <tr>

                                <th width="9%">#</th>
                                <th>Nombre</th>
                                <th width="10%">Fecha Fin</th>
                                <th width="10%">País</th>
                                <th>Ciudad</th>
                                <th width="12%">Costo</th>
                                <th width="12%">Escuela</th>
                                <th>Estado</th>
                                <th>Observaciones</th>
                            </tr>
                            <tbody>
                            @foreach($persona->curso_personas as $curso_persona)
                            <tr>
                                <td>{{++$i}}</td>
                                <td>{{$curso_persona->curso->nombre}}</td>
                                <td>{{$curso_persona->curso->fecha_fin}}</td>
                                <td>{{$curso_persona->curso->pais->nombre}}</td>
                                <td>{{$curso_persona->curso->ciudad}}</td>
                                <td>{{$curso_persona->curso->costo}}</td>
                                <td>{{$curso_persona->curso->escuela->abreviatura}}</td>
                                @if($curso_persona->estado=="A")
                                <td><span class="label label-sm label-success">Aprobado</span></td>
                                @elseif($curso_persona->estado=="R")
                                <td><span class="label label-sm label-danger">Reprobado</span></td>
                                @elseif($curso_persona->estado=="O")
                                <td><span class="label label-sm label-info">Otros</span></td>
                                @else
                                <td><span class="label label-sm label-warning">Pendiente</span></td>
                                @endif
                                <td>{{$curso_persona->observaciones}}</td>

                            </tr>
                            @endforeach
                            </tbody>
                            </thead></table>
                    </div>
                </div>
            </div>

        </div>
        <div class="form-actions text-right pal">
        <button onclick="goBack()"type="button" class="btn btn-green">Regresar</button>
            </div>
    </form>
</div>
</div>

</div>
</div>
</div>


@stop

@section('aditional_scripts')
@if(isset($personas))
<script src="{{env('APP_BASE')}}vendors/DataTables/media/js/jquery.dataTables.js"></script>
<script src="{{env('APP_BASE')}}vendors/DataTables/media/js/dataTables.bootstrap.js"></script>
<script src="{{env('APP_BASE')}}vendors/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script src="{{env('APP_BASE')}}js/table-datatables.js"></script>
@endif
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/sco.message/sco.message.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-notific8/notific8.js"></script>
<script src="{{env('APP_BASE')}}js/ui-notific8.js"></script>
<script>
    function goBack() {
        window.history.back();
    }
</script>

@stop