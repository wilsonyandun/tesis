<?php namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Auditoria;
class ProfileController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function getIndex($msg=null)
	{
        $title='Usuarios | Perfil ';
        $page_title='Perfil de Usuario';
        $nav1='Usuario';
        $nav2='Perfil';
        $usuario=User::find(Auth::user()->id);
        $action1='ProfileController@postIndex';
        $action2='ProfileController@postPassword';
		return view('usuarios.profile',array('msg'=>$msg,'action1'=>$action1,'action2'=>$action2,'title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'usuario'=>$usuario
        ));
	}
    public function postIndex(Request $request){
        $data=$request->all();
        $rules= array(
            'nombres'=>'required',
            'apellidos'=>'required',
            'email'=>'required|email'
            );
        $v=Validator::make($data,$rules);

        if($v->fails()){
            return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
            $usuario=User::find(Auth::user()->id);
            DB::enableQueryLog();
            $usuario->update($data);
            $queries = DB::getQueryLog();
            if(count($queries)>0){
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"MODIFICAR USUARIO","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
            }
            return redirect()->back()->with('msg','El usuario a sido modificado exitosamente');
        }
    }
    public function postPassword(Request $request){
        $data=$request->all();
        $rules= array(
            'password'=>'required');
        $v=Validator::make($data,$rules);
        if($v->fails()){
            return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
            $usuario=User::find(Auth::user()->id);
            DB::enableQueryLog();
            $usuario->update($data);
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"MODIFICAR CONTRASEÑA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
            return redirect()->back()->with('msg','La contraseña a sido modificado exitosamente');
        }
    }


}
