<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonaDocente extends Model {

    protected $table = 'persona_docente';

    protected $fillable = ['tipo','persona_id'];

}
