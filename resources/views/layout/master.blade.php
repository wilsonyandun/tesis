<!DOCTYPE html>
<html lang="en">
<head><title>{{$title}}</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="Thu, 19 Nov 1900 08:52:00 GMT">
    <link rel="shortcut icon" href="{{env('APP_BASE')}}images/icons/favicon.ico">
    <link rel="apple-touch-icon" href="{{env('APP_BASE')}}images/icons/favicon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{env('APP_BASE')}}images/icons/favicon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{env('APP_BASE')}}images/icons/favicon-114x114.png">
    <!--Loading bootstrap css-->
    <link type="text/css" rel="stylesheet"
          href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link type="text/css" rel="stylesheet"
          href="{{env('APP_BASE')}}vendors/jquery-ui-1.10.4.custom/css/ui-lightness/jquery-ui-1.10.4.custom.min.css">
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/font-awesome/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap/css/bootstrap.min.css">
    <!--LOADING STYLESHEET FOR PAGE-->
    @yield('aditional_css')
    <!--Loading style vendors-->
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/animate.css/animate.css">
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/jquery-pace/pace.css">
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/iCheck/skins/all.css">
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.css">
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-daterangepicker/daterangepicker-bs3.css">
    <!--Loading style-->
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}css/themes/style1/orange-blue.css" class="default-style">
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}css/themes/style1/orange-blue.css" id="theme-change"
          class="style-change color-change">
    <link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}css/style-responsive.css">
</head>
<body class=" ">
<div>
<!--BEGIN BACK TO TOP--><a id="totop" href="#"><i class="fa fa-angle-up"></i></a><!--END BACK TO TOP-->
    <!--BEGIN TOPBAR-->
    <div id="header-topbar-option-demo" class="page-header-topbar">
        <nav id="topbar" role="navigation" style="margin-bottom: 0; z-index: 2;"
             class="navbar navbar-default navbar-static-top">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target=".sidebar-collapse" class="navbar-toggle"><span
                        class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                        class="icon-bar"></span><span class="icon-bar"></span></button>
                <a id="logo" href="{{env('APP_BASE')}}" class="navbar-brand"><span class="fa fa-rocket"></span><span
                        class="logo-text">COMANDO CONJUNTO</span><span style="display: none" class="logo-text-icon">CC</span></a>
            </div>
            <div class="topbar-main" ><a id="menu-toggle" href="#" class="hidden-xs"><i class="fa fa-bars"></i></a>


                <ul class="nav navbar navbar-top-links navbar-right mbn">


                    <li class="dropdown topbar-user"><a data-hover="dropdown" href="#" class="dropdown-toggle"><img
                                src="{{env('APP_BASE')}}images/logo.jpg" alt=""
                                class="img-responsive img-circle"/>&nbsp;<span
                                class="hidden-xs">{{Auth::user()->nombres." ".Auth::user()->apellidos}}</span>&nbsp;<span
                                class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-user pull-right">
                            <li><a href="{{env('APP_BASE')}}perfil"><i class="fa fa-user"></i>Mi Perfil</a></li>

                            <li class="divider"></li>

                            <li><a href="{{env('APP_BASE')}}auth/logout"><i class="fa fa-key"></i>Salir</a></li>
                        </ul>
                    </li>


                </ul>
            </div>
        </nav>
        <!--BEGIN MODAL CONFIG PORTLET-->

        <!--END MODAL CONFIG PORTLET--></div>
    <!--END TOPBAR-->
    <div id="wrapper"><!--BEGIN SIDEBAR MENU-->
        <nav id="sidebar" role="navigation" class="navbar-default navbar-static-side">
            <div class="sidebar-collapse menu-scroll">
                <ul id="side-menu" class="nav">
                    <li class="user-panel">
                        <div class="thumb"><img src="{{env('APP_BASE')}}images/logo.jpg"
                                                alt="" class="img-circle"/></div>
                        <div class="info"><p>{{Auth::user()->nombres." ".Auth::user()->apellidos}}</p>
                            <ul class="list-inline list-unstyled">
                                <li><a href="{{env('APP_BASE')}}perfil" data-hover="tooltip" title="Perfil"><i
                                            class="fa fa-user"></i></a></li>
                                <li><a href="{{env('APP_BASE')}}auth/logout" data-hover="tooltip" title="Salir"><i
                                            class="fa fa-sign-out"></i></a></li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </li>
                    <li class="active"><a href="{{env('APP_BASE')}}"><i class="fa fa-tachometer fa-fw">
                                <div class="icon-bg bg-orange"></div>
                            </i><span class="menu-title">Dashboard</span></a></li>

                    @if(Auth::user()->tipo==0)
                    <li><a href="#"><i class="fa fa-group fa-fw">
                                <div class="icon-bg bg-yellow"></div>
                            </i><span class="menu-title">Usuarios</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{env('APP_BASE')}}usuario/lista"><i class="fa fa-list-alt"></i><span class="submenu-title">Lista de Usuarios</span></a>
                            </li>
                            <li><a href="{{env('APP_BASE')}}usuario/nuevo"><i class="fa fa-plus-square-o"></i><span class="submenu-title">Crear Usuario</span></a>
                            </li>
                        </ul>
                    </li>

                    <li><a href="#"><i class="fa fa-building-o fa-fw">
                                <div class="icon-bg bg-yellow"></div>
                            </i><span class="menu-title">Direcciones</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{env('APP_BASE')}}direccion/lista"><i class="fa fa-list-alt"></i><span class="submenu-title">Lista de Direcciones</span></a>
                            </li>
                            <li><a href="{{env('APP_BASE')}}direccion/nuevo"><i class="fa fa-plus-square-o"></i><span class="submenu-title">Crear Dirección</span></a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if(Auth::user()->tipo==1||Auth::user()->tipo==0)

                    <li><a href="#"><i class="fa fa-home fa-fw">
                                <div class="icon-bg bg-yellow"></div>
                            </i><span class="menu-title">Escuelas</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{env('APP_BASE')}}escuela/lista"><i class="fa fa-list-alt"></i><span class="submenu-title">Lista de Escuelas</span></a>
                            </li>
                            <li><a href="{{env('APP_BASE')}}escuela/nuevo"><i class="fa fa-plus-square-o"></i><span class="submenu-title">Crear Escuela</span></a>
                            </li>
                        </ul>
                    </li>



                    <li><a href="#"><i class="fa fa-fighter-jet fa-fw">
                                <div class="icon-bg bg-yellow"></div>
                            </i><span class="menu-title">Fuerzas</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{env('APP_BASE')}}fuerza/lista"><i class="fa fa-list-alt"></i><span class="submenu-title">Lista de Fuerzas</span></a>
                            </li>
                            <li><a href="{{env('APP_BASE')}}fuerza/nuevo"><i class="fa fa-plus-square-o"></i><span class="submenu-title">Crear Fuerza</span></a>
                            </li>
                        </ul>
                    </li>


                    <li><a href="#"><i class="fa fa-anchor fa-fw">
                                <div class="icon-bg bg-yellow"></div>
                            </i><span class="menu-title">Grados</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{env('APP_BASE')}}grado/lista"><i class="fa fa-list-alt"></i><span class="submenu-title">Lista de Grados</span></a>
                            </li>
                            <li><a href="{{env('APP_BASE')}}grado/nuevo"><i class="fa fa-plus-square-o"></i><span class="submenu-title">Crear Grado</span></a>
                            </li>
                        </ul>
                    </li>


                    <li><a href="#"><i class="fa fa-bolt fa-fw">
                                <div class="icon-bg bg-yellow"></div>
                            </i><span class="menu-title">Armas</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{env('APP_BASE')}}arma/lista"><i class="fa fa-list-alt"></i><span class="submenu-title">Lista de Armas</span></a>
                            </li>
                            <li><a href="{{env('APP_BASE')}}arma/nuevo"><i class="fa fa-plus-square-o"></i><span class="submenu-title">Crear Arma</span></a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if(Auth::user()->tipo==2||Auth::user()->tipo==1||Auth::user()->tipo==0)
                    <li><a href="#"><i class="glyphicon glyphicon-tower">
                                <div class="icon-bg bg-yellow"></div>
                            </i><span class="menu-title">Unidad de Origen</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{env('APP_BASE')}}unidad/lista"><i class="fa fa-list-alt"></i><span class="submenu-title">Lista de Unidades de Origen</span></a>
                            </li>
                            <li><a href="{{env('APP_BASE')}}unidad/nuevo"><i class="fa fa-plus-square-o"></i><span class="submenu-title">Crear Unidad de Origen</span></a>
                            </li>
                        </ul>
                    </li>


                    <li><a href="#"><i class="fa fa-group fa-fw">
                                <div class="icon-bg bg-yellow"></div>
                            </i><span class="menu-title">Personas</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{env('APP_BASE')}}persona/buscar"><i class="fa fa-search"></i><span class="submenu-title">Buscar Persona</span></a>
                            </li>
                            <li><a href="{{env('APP_BASE')}}persona/nuevo"><i class="fa fa-plus-square-o"></i><span class="submenu-title">Crear Persona</span></a>
                            </li>
                        </ul>
                    </li>

                    <li><a href="#"><i class="fa fa-pencil fa-fw">
                                <div class="icon-bg bg-yellow"></div>
                            </i><span class="menu-title">Cursos</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{env('APP_BASE')}}curso/lista"><i class="fa fa-list-alt"></i><span class="submenu-title">Lista de Cursos</span></a>
                            </li>
                            <li><a href="{{env('APP_BASE')}}curso/nuevo"><i class="fa fa-plus-square-o"></i><span class="submenu-title">Crear Curso</span></a>
                            </li>
                            <li><a href="{{env('APP_BASE')}}curso/matricular"><i class="fa fa-check-square-o"></i><span class="submenu-title">Matricula</span></a>
                            </li>
                            <li><a href="{{env('APP_BASE')}}curso/finalizar"><i class="fa fa-thumbs-o-up"></i><span class="submenu-title">Finalización de Curso</span></a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if(Auth::user()->tipo==3||Auth::user()->tipo==2||Auth::user()->tipo==1||Auth::user()->tipo==0)
                    <li><a href="#"><i class="fa fa-clipboard fa-fw">
                                <div class="icon-bg bg-yellow"></div>
                            </i><span class="menu-title">Reportes</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{env('APP_BASE')}}reporte/general"><i class="fa fa-bar-chart-o"></i><span class="submenu-title">Reporte General</span></a>
                            </li>
                            <li><a href="{{env('APP_BASE')}}reporte/seguimiento"><i class="fa fa-eye"></i><span class="submenu-title">Seguimiento</span></a>
                            </li>
                            <li><a href="{{env('APP_BASE')}}reporte/titulos"><i class="fa fa-book"></i><span class="submenu-title">Títulos</span></a>
                            </li>
                        </ul>
                    </li>
                    @endif


                    <!--li.charts-sum<div id="ajax-loaded-data-sidebar"></div>--></ul>
            </div>
        </nav>
        <!--END SIDEBAR MENU--><!--BEGIN CHAT FORM-->

        <!--END CHAT FORM--><!--BEGIN PAGE WRAPPER-->
        <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$page_title}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-left">
                    <li><i class="fa fa-home"></i>&nbsp;{{$nav1}}&nbsp;&nbsp;<i
                            class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>

                    <li class="active">{{$nav2}}</li>
                </ol>

            </div>
            <!--END TITLE & BREADCRUMB PAGE--><!--BEGIN CONTENT-->
            <div class="page-content">
                @yield('content')
                &nbsp;
            </div>
            <!--BEGIN FOOTER-->
            <div id="footer">
                <div class="copyright">2015 YpSoluciones Derechos Reservados</div>
            </div>
            <!--END FOOTER--><!--END PAGE WRAPPER--></div>
    </div>
    <script src="{{env('APP_BASE')}}js/jquery-1.10.2.min.js"></script>
    <script src="{{env('APP_BASE')}}js/jquery-migrate-1.2.1.min.js"></script>
    <script src="{{env('APP_BASE')}}js/jquery-ui.js"></script>
    <!--loading bootstrap js-->
    <script src="{{env('APP_BASE')}}vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{env('APP_BASE')}}vendors/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js"></script>
    <script src="{{env('APP_BASE')}}js/html5shiv.js"></script>
    <script src="{{env('APP_BASE')}}js/respond.min.js"></script>
    <script src="{{env('APP_BASE')}}vendors/metisMenu/jquery.metisMenu.js"></script>
    <script src="{{env('APP_BASE')}}vendors/slimScroll/jquery.slimscroll.js"></script>
    <script src="{{env('APP_BASE')}}vendors/jquery-cookie/jquery.cookie.js"></script>
    <script src="{{env('APP_BASE')}}vendors/iCheck/icheck.min.js"></script>
    <script src="{{env('APP_BASE')}}vendors/iCheck/custom.min.js"></script>
    <script src="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.js"></script>
    <script src="{{env('APP_BASE')}}vendors/jquery-highcharts/highcharts.js"></script>
    <script src="{{env('APP_BASE')}}js/jquery.menu.js"></script>
    <script src="{{env('APP_BASE')}}vendors/jquery-pace/pace.min.js"></script>
    <script src="{{env('APP_BASE')}}vendors/holder/holder.js"></script>
    <script src="{{env('APP_BASE')}}vendors/responsive-tabs/responsive-tabs.js"></script>
    <script src="{{env('APP_BASE')}}vendors/jquery-news-ticker/jquery.newsTicker.min.js"></script>
    <script src="{{env('APP_BASE')}}vendors/moment/moment.js"></script>
    <script src="{{env('APP_BASE')}}vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="{{env('APP_BASE')}}vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!--CORE JAVASCRIPT-->
    <script src="{{env('APP_BASE')}}js/main.js"></script>
    <!--LOADING SCRIPTS FOR PAGE-->

    @yield('aditional_scripts')

</body>
</html>