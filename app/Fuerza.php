<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Fuerza extends Model {

    protected $table = 'fuerzas';
    protected $fillable = ['nombre','abreviatura','activo'];
    public function grados()
    {
        return $this->hasMany('App\Grado');
    }

}
