@extends('layout.master')
@section('aditional_css')
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/animate.css/animate.css"
      xmlns="http://www.w3.org/1999/html">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/jquery-pace/pace.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/iCheck/skins/all.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/iCheck/skins/all.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-colorpicker/css/colorpicker.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-datepicker/css/datepicker.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-daterangepicker/daterangepicker-bs3.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-clockface/css/clockface.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-switch/css/bootstrap-switch.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/select2/select2-madmin.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-select/bootstrap-select.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/multi-select/css/multi-select-madmin.css">
@stop
@section('content')

<div class="col-lg-12">
<div class="panel panel-blue">
    <div class="panel-heading">{{$page_title}}</div>
    <div class="panel-body pan">

        @if(Session::get('msg'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
            <h4 style="color: #000000">El curso a sido {{Session::get('msg')}} exitosamente</h4>
        </div>
        @endif
        {!! Form::model($curso,['action'=>$action,'method'=>'POST','id'=>'form_data','class'=>'form-horizontal form-bordered form-validate']) !!}
        @if(isset($curso->id))
        <input name="curso_id" type="hidden" value="{{$curso->id}}">
        @endif
            <div class="form-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    Hay problemas con la información que ingresaste.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="form-group">

                    <label for="nombre"
                                               class="col-md-3 control-label">
                        Nombre <span class='require'>*</span></label>

                    <div class="col-md-4"><input name="nombre" minlength="2" id="nombre" type="text"
                                                 placeholder="" value="{{$curso->nombre}}"
                                                 class="form-control required "/><span
                            class="help-block">Ej: CURSO DE PARACAIDISMO </span></div>
                </div>
                <div class="form-group">
                    <label for="fecha_inicio"
                           class="col-md-3 control-label">
                        Fecha de Inicio <span class='require'>*</span></label>

                    <div class="col-md-3"><input name="fecha_inicio" type="text" data-date-format="yyyy-mm-dd"
                                                 placeholder="yyyy-mm-dd" value="{{$curso->fecha_inicio}}"
                                                 class="datepicker-default form-control required"/>
                        <span
                            class="help-block">Ej: 2014-12-15</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="fecha_fin"
                           class="col-md-3 control-label">
                        Fecha de Fin <span class='require'>*</span></label>

                    <div class="col-md-3"><input name="fecha_fin" type="text" data-date-format="yyyy-mm-dd"
                                                 placeholder="yyyy-mm-dd" value="{{$curso->fecha_fin}}"
                                                 class="datepicker-default form-control required"/>
                        <span
                            class="help-block">Ej: 2015-11-16</span>
                    </div>
                </div>
                <div class="form-group">

                    <label for="costo"
                           class="col-md-3 control-label">
                        Costo <span class='require'>*</span></label>

                    <div class="col-md-4"><input name="costo" minlength="2" id="costo" type="text"
                                                 placeholder="" value="{{$curso->nombre}}"
                                                 class="form-control required number"/><span
                            class="help-block">Ej: 250.00 </span></div>
                </div>
                <div class="form-group"><label for="pais_id"
                                               class="col-md-3 control-label">
                        País <span class='require'>*</span></label>

                    <div class="col-md-6 col-sm-6 col-xs-6">{!!Form::select('pais_id', $paises, $curso->pais_id,array("class"=>"select2-size form-control")) !!}
                        <span
                            class="help-block"></span></div>
                </div>
                <div class="form-group">

                    <label for="nombre"
                           class="col-md-3 control-label">
                        Ciudad <span class='require'>*</span></label>

                    <div class="col-md-4"><input name="ciudad" minlength="2" id="ciudad" type="text"
                                                 placeholder="" value="{{$curso->ciudad}}"
                                                 class="form-control required "/><span
                            class="help-block">Ej: QUITO </span></div>
                    </div>
                <div class="form-group"><label for="escuela_id"
                                               class="col-md-3 control-label">
                        Escuela <span class='require'>*</span></label>

                    <div class="col-md-6 col-sm-6 col-xs-6">{!!Form::select('escuela_id', $escuelas, $curso->escuela_id,array("class"=>"select2-size form-control")) !!}
                        <span
                            class="help-block"></span></div>
                </div>
                <div class="form-group"><label for="tipo"
                                               class="col-md-3 control-label">
                        Tipo <span class='require'>*</span></label>

                    <div class="col-md-6 col-sm-6 col-xs-6">{!!Form::select('tipo', $tipos,' ',array("class"=>"select2-size form-control","id"=>"tipo")) !!}
                        <span
                            class="help-block"></span></div>
                </div>

                <div class="form-group">

                    <label for="descripcion"
                           class="col-md-3 control-label">
                        Descripción <span class='require'>*</span></label>

                    <div class="col-md-4"><textarea name="descripcion" minlength="2" id="descripcion"  rows="6"

                                                 class="form-control required " /></textarea><span
                            class="help-block">Ej: CURSO AVANZADO SOBRE TÉCNICAS </span></div>
                </div>
                <div class="form-group">

                    <label for="institucion_destino"
                           class="col-md-3 control-label">
                        Institución Destino <span class='require'>*</span></label>

                    <div class="col-md-4"><input name="institucion_destino" minlength="2" id="institucion_destino" type="text"
                                                 placeholder="" value=""
                                                 class="form-control required "/><span
                            class="help-block">Ej: ESMIL </span></div>
                </div>
                <div class="form-group">

                    <label for="observaciones"
                           class="col-md-3 control-label">
                        Observaciones </label>

                    <div class="col-md-4"><textarea name="observaciones"  id="observaciones" type="textarea" rows="6"
                                                 placeholder="" value=""
                                                 class="form-control  "/></textarea><span
                            class="help-block"> </span></div>
                </div>
            </div>
            <div class="form-actions text-right pal">
                <button id="enviar" type="submit" class="btn btn-primary" disabled>Guardar</button>
                &nbsp;
                <button type="button" class="btn btn-green">Cancelar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
</div>

@stop
@section('aditional_scripts')
<script>
    $('#enviar').on('click', function(e) {
        $('#enviar').attr("disabled","true");
        $('#enviar').html("Guardando");
        $('#form_data').submit();
    });
    $('#form_data').bind('change keyup', function() {
        if($('#form_data').validate().checkForm()) {
            $('#enviar').attr('disabled', false);
        } else {
            $('#enviar').attr('disabled', true);
        } });

</script>
<script src="{{env('APP_BASE')}}vendors/iCheck/icheck.js"></script>
<script src="{{env('APP_BASE')}}vendors/iCheck/custom.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-validate/jquery.validate.min.js"></script>
<script src="{{env('APP_BASE')}}js/form-validation.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="{{env('APP_BASE')}}vendors/moment/moment.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-clockface/js/clockface.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/jquery-maskedinput/jquery-maskedinput.js"></script>
<script src="{{env('APP_BASE')}}vendors/charCount.js"></script>
<script src="{{env('APP_BASE')}}js/form-components.js"></script>
<script src="{{env('APP_BASE')}}vendors/select2/select2.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="{{env('APP_BASE')}}vendors/multi-select/js/jquery.multi-select.js"></script>
<script src="{{env('APP_BASE')}}js/ui-dropdown-select.js"></script>
@stop