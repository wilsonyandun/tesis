@extends('layout.master')
@section('aditional_css')
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/animate.css/animate.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/jquery-pace/pace.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/iCheck/skins/all.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/jquery-notific8/jquery.notific8.min.css">
<link type="text/css" rel="stylesheet" href="{{env('APP_BASE')}}vendors/bootstrap-daterangepicker/daterangepicker-bs3.css">
@stop
@section('content')

<div class="col-lg-12">
<div class="panel panel-blue">
    <div class="panel-heading">{{$page_title}}</div>
    <div class="panel-body pan">

        @if(Session::get('msg'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
            <h4 style="color: #000000">El arma ha sido {{Session::get('msg')}} exitosamente</h4>
        </div>
        @endif
        {!! Form::model($arma,['action'=>$action,'method'=>'POST','id'=>'form_data','class'=>'form-horizontal form-bordered form-validate']) !!}
        @if(isset($arma->id))
        <input name="arma_id" type="hidden" value="{{$arma->id}}">
        @endif
            <div class="form-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    Hay problemas con la información que ingresaste<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="form-group">

                    <label for="nombre"
                                               class="col-md-3 control-label">
                        Nombre <span class='require'>*</span></label>

                    <div class="col-md-4"><input name="nombre" minlength="2" id="nombre" type="text"
                                                 placeholder="" value="{{$arma->nombre}}"
                                                 class="form-control required "/><span
                            class="help-block">Ej: DIRECCIÓN DE EDUCACIÓN Y DOCTRINA MILITAR </span></div>
                </div>
                <div class="form-group"><label for="abreviatura"
                                               class="col-md-3 control-label">
                        Abreviatura <span class='require'>*</span></label>

                    <div class="col-md-4"><input name="abreviatura" id="abreviatura" type="text"
                                                 placeholder=""
                                                 value="{{$arma->abreviatura}}"
                                                 class="form-control required "/><span
                            class="help-block">Ej: DIEDMIL </span></div>
                </div>
            </div>
            <div class="form-actions text-right pal">
                <button id="enviar" type="submit" class="btn btn-primary" disabled>Guardar</button>
                &nbsp;
                <button type="button" class="btn btn-green">Cancelar</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
</div>

@stop
@section('aditional_scripts')
<script>
    $('#enviar').on('click', function(e) {
        $('#enviar').attr("disabled","true");
        $('#enviar').html("Guardando");
        $('#form_data').submit();
    });
    $('#form_data').bind('change keyup', function() {
        if($('#form_data').validate().checkForm()) {
            $('#enviar').attr('disabled', false);
        } else {
            $('#enviar').attr('disabled', true);
        } });

</script>
<script src="{{env('APP_BASE')}}vendors/jquery-validate/jquery.validate.min.js"></script>
<script src="{{env('APP_BASE')}}js/form-validation.js"></script>
@stop