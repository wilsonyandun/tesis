<?php namespace App\Http\Controllers;
use App\Unidad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Auditoria;

class UnidadController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('admin_escuela');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function getLista($msg=null)
	{
        $title='Unidades | Lista ';
        $page_title='Lista de Unidades';
        $nav1='Unidades';
        $nav2='Lista';
        $unidades=Unidad::all();
		return view('unidad.list',array('msg'=>$msg,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'unidades'=>$unidades
        ));
	}
    public function getNuevo(){
        $title='Unidades | Crear ';
        $page_title='Crear de Unidad';
        $nav1='Unidades';
        $nav2='Crear';
        $action='UnidadController@postNuevo';
        $unidad=new Unidad();
        return view('unidad.form',array('action'=>$action,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'unidad'=>$unidad
        ));
    }
    public function postNuevo(Request $request){
        $data=$request->all();
        $rules= array(
            'nombre'=>'required|unique:unidades',
            'abreviatura'=>'required');
        $v=Validator::make($data,$rules);
        if($v->fails()){
           return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
            DB::enableQueryLog();
        $unidad= Unidad::create($request->all());
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"CREAR UNIDAD","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
        return redirect()->back()->with('msg','creado');
        }
    }
    public function getEditar($id){
        $title='Unidades | Editar ';
        $page_title='Edición de Dirección';
        $nav1='Unidades';
        $nav2='Crear';
        $action='UnidadController@postEditar';
        $unidad=Unidad::find($id);
        return view('unidad.form',array('action'=>$action,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'unidad'=>$unidad
        ));
    }
    public function postEditar(Request $request){
        $data=$request->all();
        $rules= array(
            'nombre'=>'required|unique:unidades,nombre,'.$request['unidad_id'],
            'abreviatura'=>'required');
        $v=Validator::make($data,$rules);
        if($v->fails()){
            return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
        $unidad=Unidad::find($request['unidad_id']);
            DB::enableQueryLog();
        $unidad->update($request->all());
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"MODIFICAR UNIDAD","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
        return redirect()->back()->with('msg','modificado');
        }
    }
    public function postEliminar(Request $request){
        $unidad=Unidad::find($request['id']);
        $data=array("activo"=>"0");
        DB::enableQueryLog();
        $unidad->update($data);
        $queries = DB::getQueryLog();
        $last_query = end($queries);
        $last_query=implode(" , ", array_flatten($last_query));
        DB::disableQueryLog();
        $info=array("ip"=>$request->getClientIp(),"accion"=>"ELIMINAR UNIDAD","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
        $auditoria=Auditoria::create($info);
        $validate=true;
        echo json_encode(array('validate'=>$validate));
    }


}
