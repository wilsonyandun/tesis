<?php
/**
 * Created by PhpStorm.
 * User: Marketing
 * Date: 15/03/15
 * Time: 08:47 PM
 */
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class UsuarioTableSeeder extends Seeder{
public function run()
{

    $faker= Faker::create();
    DB::table('usuarios')->insert(array(
        'nombres'=>'Wilson',
        'apellidos'=>'Yandún',
        'username'=>'admin',
        'email'=>'admin@ypsoluciones.com',
        'tipo'=>'0',
        'password'=>Hash::make('1234'),
        'escuela_id'=>'1',
         'activo'=>'1'
    ));
    for($i=0;$i<100;$i++){
        DB::table('usuarios')->insert(array(
            'nombres'=>$faker->firstName,
            'apellidos'=>$faker->lastName,
            'username'=>$faker->unique()->username,
            'email'=>$faker->unique()->email,
            'tipo'=>$faker->numberBetween($min = 1, $max = 3),
            'password'=>Hash::make('1234'),
            'escuela_id'=>$faker->numberBetween($min = 2, $max = 4),
            'activo'=>'1'
        ));
    }
}
}