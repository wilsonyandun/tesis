<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Arma extends Model {

    protected $table = 'armas';

    protected $fillable = ['nombre','abreviatura','activo'];

    public function arma_persona()
    {
        return $this->hasMany('App\ArmaPersona');
    }

    public function scopePersonaId($query, $type)
    {
        return $query->wherePersonaId($type);
    }

}
