<?php namespace App\Http\Controllers;
use App\Grado;
use App\GradoPersona;
use App\Persona;
use App\Arma;
use App\Titulo;
use App\Unidad;
use App\Nacionalidad;
use App\PersonaAlumno;
use App\PersonaAdministrativo;
use App\PersonaDocente;
use App\ArmaPersona;
use App\UnidadPersona;
use App\Auditoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use DB;
class PersonaController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('admin_escuela');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function getBuscar($msg=null)
	{
        $title='Personas | Búsqueda ';
        $page_title='Búsqueda de Persona';
        $nav1='Personas';
        $nav2='Búsqueda';
        $persona=new Persona();
        $action='PersonaController@postBuscar';
		return view('persona.search',array('action'=>$action,'persona'=>$persona,'msg'=>$msg,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ));
	}
    public function postBuscar(Request $request){
        $data=$request->all();
        $persona=new Persona();
        $persona->nombres=$data['nombres'];
        $persona->apellidos=$data['apellidos'];
        $persona->identificacion=$data['identificacion'];
        $title='Personas | Búsqueda ';
        $page_title='Búsqueda de Persona';
        $nav1='Personas';
        $nav2='Búsqueda';
        $personas=Persona::where('nombres', 'like', '%'.$data['nombres'].'%')
            ->where('apellidos', 'like', '%'.$data['apellidos'].'%')
            ->where('identificacion', 'like', '%'.$data['identificacion'].'%')
            ->get();

        $action='PersonaController@postBuscar';
        return view('persona.search',array('action'=>$action,'persona'=>$persona,'personas'=>$personas,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2));

    }
    public function getNuevo(){
        $title='Personas | Crear ';
        $page_title='Creación de Persona';
        $nav1='Personas';
        $nav2='Crear';
        $action='PersonaController@postNuevo';
        $persona=new Persona();
        $grados=Grado::todos();
        $grados_all=array();
        foreach($grados as $grado){
            $grados_all=array_add($grados_all,$grado->id,$grado->nombre);
        }
        $armas=Arma::all();
        $armas=$armas->lists('nombre','id');
        $nacionalidades=Nacionalidad::all();
        $nacionalidades=$nacionalidades->lists('nombre','id');
        $unidades=Unidad::all();
        $unidades=$unidades->lists('nombre','id');
        return view('persona.form',array('nacionalidades'=>$nacionalidades,'unidades'=>$unidades,'armas'=>$armas,'grados'=>$grados_all,'action'=>$action,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'persona'=>$persona
        ));
    }
    public function postNuevo(Request $request){
        $data=$request->all();
        $rules= array(
            'nombres'=>'required',
            'apellidos'=>'required',
            'identificacion'=>'required|unique:personas',
            'email'=>'required',
            'direccion'=>'required',
            'telefono'=>'required',
            'tipo'=>'required');
        $v=Validator::make($data,$rules);
        if($v->fails()){
           return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($data);
        }else{
            DB::enableQueryLog();
            $persona= Persona::create($data);
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"CREAR PERSONA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
            if(isset($data["grado_id"])){
                $info=array('persona_id'=>$persona->id,'grado_id'=>$data["grado_id"]);
                $grado_persona=GradoPersona::create($info);
            }
            if(isset($data["arma_id"])){
                $info=array('persona_id'=>$persona->id,'arma_id'=>$data["arma_id"]);
                $arma_persona=ArmaPersona::create($info);
            }
            if(isset($data["grado_id"])){
                $info=array('persona_id'=>$persona->id,'unidad_id'=>$data["unidad_id"]);
                $unidad_persona=UnidadPersona::create($info);
            }
            if(count($data["tipo"])>0){
                foreach($data["tipo"] as $tipo){
                    if($tipo=="alumno"){
                        $info=array('persona_id'=>$persona->id);
                        $alumno=PersonaAlumno::create($info);
                    }
                    if($tipo=="administrativo"){
                        $info=array('persona_id'=>$persona->id);
                        $administrativo=PersonaAdministrativo::create($info);

                    }
                    if($tipo=="docente"){
                        $info=array('persona_id'=>$persona->id,'tipo'=>$data['tipo_docente']);
                        $docente=PersonaDocente::create($info);
                    }
                }
            }
            if(count($data["tipo_titulo"])>0){
                for($i=0;$i<count($data["tipo_titulo"]);$i++){
                    $info=array("persona_id"=>$persona->id,"tipo"=>$data["tipo_titulo"][$i],"nombre"=>$data["nombre_titulo"][$i],"institucion"=>$data["institucion_titulo"][$i]);
                    $titulo=Titulo::create($info);
                }
            }
        return redirect()->back()->with('msg','creado');
        }
    }
    public function getEditar($id){
        $title='Personas | Editar ';
        $page_title='Edición de Persona';
        $nav1='Personas';
        $nav2='Editar';
        $action='PersonaController@postEditar';
        $persona=Persona::find($id);
        $grados=Grado::todos();
        $grados_all=array();
        foreach($grados as $grado){
            $grados_all=array_add($grados_all,$grado->id,$grado->nombre);
        }
        $armas=Arma::all();
        $armas=$armas->lists('nombre','id');
        $nacionalidades=Nacionalidad::all();
        $nacionalidades=$nacionalidades->lists('nombre','id');
        $unidades=Unidad::all();
        $unidades=$unidades->lists('nombre','id');
        return view('persona.form',array('nacionalidades'=>$nacionalidades,'unidades'=>$unidades,'armas'=>$armas,'grados'=>$grados_all,'action'=>$action,'i'=>'0','title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2
        ,'persona'=>$persona
        ));
    }
    public function postEditar(Request $request){
        $data=$request->all();
        $rules= array(
                'nombres'=>'required',
                'apellidos'=>'required',
                'identificacion'=>'required|unique:personas',
                'email'=>'required',
                'direccion'=>'required',
                'telefono'=>'required',
                'tipo'=>'required');
        $v=Validator::make($data,$rules);
        if($v->fails()){
            return redirect()->back()
                ->withErrors($v->errors())
                ->withInput($request->all());
        }else{
        $persona=Persona::find($request['persona_id']);
            DB::enableQueryLog();
        $persona->update($request->all());
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $last_query=implode(" , ", array_flatten($last_query));
            DB::disableQueryLog();
            $info=array("ip"=>$request->getClientIp(),"accion"=>"MODIFICAR PERSONA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
            $auditoria=Auditoria::create($info);
        return redirect()->back()->with('msg','modificado');
        }
    }
    public function postEliminar(Request $request){
        $persona=Persona::find($request['id']);
        $data=array("activo"=>"0");
        DB::enableQueryLog();
        $persona->update($data);
        $queries = DB::getQueryLog();
        $last_query = end($queries);
        $last_query=implode(" , ", array_flatten($last_query));
        DB::disableQueryLog();
        $info=array("ip"=>$request->getClientIp(),"accion"=>"ELIMINAR PERSONA","sql"=>$last_query,"usuario_id"=>Auth::user()->id);
        $auditoria=Auditoria::create($info);
        $validate=true;
        echo json_encode(array('validate'=>$validate));
    }
    public function getVer($id){
        $title='Persona | Ver ';
        $page_title='Información de Persona';
        $nav1='Personas';
        $nav2='Información';
        $persona=Persona::find($id);

        return view('persona.view',array('persona'=>$persona,'title'=>$title,'page_title'=>$page_title,'nav1'=>$nav1,'nav2'=>$nav2));
    }


}
