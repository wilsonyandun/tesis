<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonaAdministrativo extends Model {

    protected $table = 'persona_administrativo';

    protected $fillable = ['persona_id'];

}
