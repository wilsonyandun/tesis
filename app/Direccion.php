<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model {

    protected $table = 'direcciones';

    protected $fillable = ['nombre','abreviatura','activo'];
    public function escuelas()
    {
        return $this->hasMany('App\Escuela');
    }
}
