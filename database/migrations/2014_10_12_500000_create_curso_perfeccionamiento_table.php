<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursoPerfeccionamientoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cursos_perfeccionamiento', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('observaciones');
            $table->string('institucion_destino');
            $table->string('tipo');
            $table->string('descripcion');
            $table->integer('curso_id')->unsigned();
            $table->foreign('curso_id')->references('id')->on('cursos');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cursos_perfeccionamiento');
	}

}
