<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ArmaPersona extends Model {

    protected $table = 'arma_persona';
    protected $fillable = ['arma_id','persona_id'];

    public function persona()
    {
        return $this->belongsTo('App\Persona');
    }
    public function arma(){
        return $this->belongsTo('App\Arma');
    }


}
