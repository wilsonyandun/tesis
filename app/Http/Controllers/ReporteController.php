<?php namespace App\Http\Controllers;

use App\Arma;
use App\Curso;
use App\CursoPersona;
use App\Direccion;
use App\Grado;
use App\Titulo;
use Carbon\Carbon;

use App\Escuela;
use App\Nacionalidad;
use App\Persona;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Faker\Provider\fr_BE\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReporteController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('user');

    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function getGeneral()
    {
        $title = 'Reportes | General ';
        $page_title = 'Reporte General';
        $nav1 = 'Reportes';
        $nav2 = 'General';
        $now = Carbon::now();
        $data = array('endstart' => $now->toDateString(), 'datestart' => $now->toDateString(), 'direccion_id' => '0', 'escuela_id' => '0', 'tipos' => '0', 'curso_id' => '0', 'atributos' => '0', 'valores' => '0');
        $action = 'ReporteController@postGeneral';
        $cursos=new Collection();
        if(Auth::user()->tipo=='0'){
            $direcciones = Direccion::all();
            $escuelas = Escuela::all();
            $cursos = Curso::all();
            $direcciones = array(0 => 'TODAS') + $direcciones->lists('nombre', 'id');
            $escuelas = array(0 => 'TODAS') + $escuelas->lists('nombre', 'id');
            $cursos = array(0 => 'TODOS') + $cursos->lists('nombre', 'id');
        }elseif(Auth::user()->tipo=='1'){
            $direcciones=Auth::user()->escuela->direccion()->get();
            $escuelas=Escuela::where('direccion_id','=',$direcciones[0]->id)->get();
            $direcciones = $direcciones->lists('nombre', 'id');
            $escuelas = array(0 => 'TODAS') + $escuelas->lists('nombre', 'id');
            foreach($escuelas as $escuela){
                $cursos=$cursos->merge(Curso::where("escuela_id", '=', $escuela->id)->get());
            }
            $cursos = array(0 => 'TODOS') + $cursos->lists('nombre', 'id');

        }else{
            $direcciones=Auth::user()->escuela->direccion()->get();
            $direcciones = $direcciones->lists('nombre', 'id');
            $escuelas=Escuela::where('id','=',Auth::user()->escuela_id)->get();
            $escuelas = $escuelas->lists('nombre', 'id');
            $cursos=$cursos->merge(Curso::where("escuela_id", '=',Auth::user()->escuela_id)->get());
            $cursos = array(0 => 'TODOS') + $cursos->lists('nombre', 'id');
        }
        $tipos = array('0' => 'TODOS', '1' => 'CAPACITACIÓN', '2' => 'ESPECIALIZACIÓN', '3' => 'FORMACIÓN', '4' => 'PERFECCIONAMIENTO', '5' => 'EDUCACIÓN REGULAR');
        $atributos = array('0' => 'TODOS', '1' => 'GÉNERO', '2' => 'NACIONALIDAD', '3' => 'CALIFICACIÓN');
        $valores = array('0' => 'TODOS');
        $val1 = "";
        $val2 = "";
        $val3 = "";
        return view('reporte.general', array('val1' => $val1, 'val2' => $val2, 'val3' => $val3, 'valores' => $valores, 'atributos' => $atributos, 'cursos' => $cursos, 'tipos' => $tipos, 'direcciones' => $direcciones, 'escuelas' => $escuelas, 'action' => $action, 'data' => $data, 'i' => '0', 'title' => $title, 'page_title' => $page_title, 'nav1' => $nav1, 'nav2' => $nav2
        , 'cursos' => $cursos
        ));
    }

    public function postGeneral(Request $request)
    {
        $title = 'Reportes | General ';
        $val1 = 0;
        $val2 = 0;
        $val3 = 0;
        $page_title = 'Reporte General';
        $nav1 = 'Reportes';
        $nav2 = 'General';
        $data = $request->all();
        $cursos_reporte = new Collection();
        $action = 'ReporteController@postGeneral';
        if ($data['direccion_id'] == 0 && $data['escuela_id'] == 0 && $data['tipos'] == 0 && $data['curso_id'] == 0) {
            $cursos_reporte = Curso::where('fecha_fin', '>=', array($data['datestart']))->where('fecha_fin', '<=', $data['endstart'])->get();
            foreach ($cursos_reporte as $reporte) {
                $val1 = $val1 + count($reporte->curso_persona);
            }
            $series=array(array("nombre"=>"Personas","data"=>$val1));
            $val2 = "";
            $val3 = "";
        } elseif ($data['direccion_id'] == 0 && $data['escuela_id'] == 0 && $data['curso_id'] == 0) {
            $cursos = Curso::where('fecha_fin', '>=', array($data['datestart']))->where('fecha_fin', '<=', $data['endstart'])->get();
            foreach ($cursos as $curso) {
                if (($data['tipos'] == 1 && count($curso->curso_capacitacion) > 0) || ($data['tipos'] == 2 && count($curso->curso_especializacion) > 0) || ($data['tipos'] == 3 && count($curso->curso_formacion) > 0) || ($data['tipos'] == 4 && count($curso->curso_perfeccionamiento) > 0) || ($data['tipos'] == 5 && count($curso->curso_educacion_regular) > 0)) {
                    $cursos_reporte = $cursos_reporte->merge(Curso::where('id', '=', $curso->id)->get());
                }
            }
            foreach ($cursos_reporte as $curso) {
                $val1 = $val1 + count($curso->curso_persona);
            }
            $val2 = "";
            $val3 = "";
            $series=array(array("nombre"=>"Personas","data"=>$val1));
        } elseif ($data['escuela_id'] == 0 && $data['tipos'] == 0 && $data['curso_id'] == 0) {
            $lista_escuelas = Escuela::where('direccion_id', '=', $data['direccion_id'])->get();
            foreach ($lista_escuelas as $escuela) {
                $cursos = Curso::where('fecha_fin', '>=', array($data['datestart']))->where('fecha_fin', '<=', $data['endstart'])->where("escuela_id", '=', $escuela->id)->get();
                $cursos_reporte = $cursos_reporte->merge($cursos);
            }
            foreach ($cursos_reporte as $curso) {
                $val1 = $val1 + count($curso->curso_persona);
            }
            $series=array(array("nombre"=>"Personas","data"=>$val1));
            $val2 = "";
            $val3 = "";
        } elseif ($data['escuela_id'] == 0 && $data['curso_id'] == 0) {
            $lista_escuelas = Escuela::where('direccion_id', '=', $data['direccion_id'])->get();
            foreach ($lista_escuelas as $escuela) {
                $cursos = Curso::where('fecha_fin', '>=', array($data['datestart']))->where('fecha_fin', '<=', $data['endstart'])->where("escuela_id", '=', $escuela->id)->get();
                foreach ($cursos as $curso) {
                    if (($data['tipos'] == 1 && count($curso->curso_capacitacion) > 0) || ($data['tipos'] == 2 && count($curso->curso_especializacion) > 0) || ($data['tipos'] == 3 && count($curso->curso_formacion) > 0) || ($data['tipos'] == 4 && count($curso->curso_perfeccionamiento) > 0) || ($data['tipos'] == 5 && count($curso->curso_educacion_regular) > 0)) {
                        $cursos_reporte = $cursos_reporte->merge(Curso::where('id', '=', $curso->id)->get());
                    }
                }
            }
            foreach ($cursos_reporte as $curso) {
                $val1 = $val1 + count($curso->curso_persona);
            }
            $series=array(array("nombre"=>"Personas","data"=>$val1));
            $val2 = "";
            $val3 = "";
        } elseif ($data['tipos'] == 0 && $data['curso_id'] == 0) {
            $cursos_reporte = Curso::where('fecha_fin', '>=', array($data['datestart']))->where('fecha_fin', '<=', $data['endstart'])->where("escuela_id", '=', $data['escuela_id'])->get();
            foreach ($cursos_reporte as $curso) {
                $val1 = $val1 + count($curso->curso_persona);
            }
            $series=array(array("nombre"=>"Personas","data"=>$val1));
            $val2 = "";
            $val3 = "";
        } elseif ($data['curso_id'] == 0) {
            $cursos = Curso::where('fecha_fin', '>=', array($data['datestart']))->where('fecha_fin', '<=', $data['endstart'])->where("escuela_id", '=', $data['escuela_id'])->get();
            foreach ($cursos as $curso) {
                if (($data['tipos'] == 1 && count($curso->curso_capacitacion) > 0) || ($data['tipos'] == 2 && count($curso->curso_especializacion) > 0) || ($data['tipos'] == 3 && count($curso->curso_formacion) > 0) || ($data['tipos'] == 4 && count($curso->curso_perfeccionamiento) > 0) || ($data['tipos'] == 5 && count($curso->curso_educacion_regular) > 0)) {
                    $cursos_reporte = $cursos_reporte->merge(Curso::where('id', '=', $curso->id)->get());
                }
            }
            foreach ($cursos_reporte as $curso) {
                $val1 = $val1 + count($curso->curso_persona);
            }
            $series=array(array("nombre"=>"Personas","data"=>$val1));
            $val2 = "";
            $val3 = "";
        } else {
            $cursos_reporte = Curso::where('fecha_fin', '>=', array($data['datestart']))->where('fecha_fin', '<=', $data['endstart'])->where('id', '=', $data['curso_id'])->get();
            foreach ($cursos_reporte as $curso) {
                $val1 = $val1 + count($curso->curso_persona);
            }
            $series=array(array("nombre"=>"Personas","data"=>$val1));
            $val2 = "";
            $val3 = "";
        }
        $valores = array('0' => 'TODOS');
        if ($data['atributos'] == 1) {
            $val1=0;
            $val2=0;
            $valores = array('0' => 'TODOS', 'H' => "HOMBRE", "M" => "MUJER");
            foreach($cursos_reporte as $c){
                $c=Curso::find($c->id);
                if($data['valores']=='0'){
                    $val1=$val1+count($c->alumnos()->hombres()->get());
                    $val2=$val2+count($c->alumnos()->mujeres()->get());
                    $series=array(array("nombre"=>"Hombres","data"=>$val1),array("nombre"=>"Mujeres","data"=>$val2));
                }
                elseif($data['valores']=='H'){
                    $val1=$val1+count($c->alumnos()->hombres()->get());
                    $series=array(array("nombre"=>"Hombres","data"=>$val1));
                    $val2="";
                }elseif($data['valores']=='M'){
                    $val1=$val1+count($c->alumnos()->mujeres()->get());
                    $series=array(array("nombre"=>"Mujeres","data"=>$val1));
                    $val2="";
                }
            }
        }
        if ($data['atributos'] == 2) {
            $valores = Nacionalidad::all();
            $valores = array('0' => 'TODOS', 'N' => "NACIONALES", "E" => "EXTRANJEROS");
            $val1=0;
            $val2=0;
            foreach($cursos_reporte as $c){
                $c=Curso::find($c->id);
                if($data['valores']=='0'){
                    $val1=$val1+count($c->alumnos()->nacionales()->get());
                    $val2=$val2+count($c->alumnos()->get())-count($c->alumnos()->nacionales()->get());
                    $series=array(array("nombre"=>"Nacionales","data"=>$val1),array("nombre"=>"Extranjeros","data"=>$val2));
                }
                elseif($data['valores']=='N'){
                    $val1=$val1+count($c->alumnos()->nacionales()->get());
                    $val2="";
                    $series=array(array("nombre"=>"Nacionales","data"=>$val1));
                }elseif($data['valores']=='E'){
                    $val1=$val1+count($c->alumnos()->get())-count($c->alumnos()->nacionales()->get());
                    $val2="";
                    $series=array(array("nombre"=>"Extranjeros","data"=>$val1));
                }
            }
        }
        if ($data['atributos'] == 3) {
            $valores = array('0' => 'TODOS', 'A' => 'APROBADO', 'R' => 'REPROBADO', 'O' => 'OTROS');
            $val1=0;
            $val2=0;
            $val3=0;
            foreach($cursos_reporte as $c){
                $c=Curso::find($c->id);
                if($data['valores']=='0'){
                    $val1=$val1+count($c->curso_persona()->aprobados()->get());
                    $val2=$val2+count($c->curso_persona()->reprobados()->get());
                    $val3=$val3+count($c->curso_persona()->otros()->get());
                    $series=array(array("nombre"=>"Aprobados","data"=>$val1),array("nombre"=>"Reprobados","data"=>$val2),array("nombre"=>"Otros","data"=>$val3));
                }
                elseif($data['valores']=='A'){
                    $val1=$val1+count($c->curso_persona()->aprobados()->get());
                    $val2="";
                    $val3="";
                    $series=array(array("nombre"=>"Aprobados","data"=>$val1));
                }elseif($data['valores']=='R'){
                    $val1=$val1+count($c->curso_persona()->reprobados()->get());
                    $val2="";
                    $val3="";
                    $series=array(array("nombre"=>"Reprobados","data"=>$val1));
                }
                elseif($data['valores']=='O'){
                    $val1=$val1+count($c->curso_persona()->otros()->get());
                    $val2="";
                    $val3="";
                    $series=array(array("nombre"=>"Otros","data"=>$val1));
                }
            }


        }
       /* if ($data['atributos'] == 4) {
            $valores=Arma::all();
            $valores=array(0 => 'TODOS')+$valores->lists('nombre','id');
            $val1=0;
            $val2=0;
            $val3=0;
            foreach($cursos_reporte as $c){
                $c=Curso::find($c->id);
                if($data['valores']=='0'){
                    $val1=$val1+count($c->alumnos());
                    $series=array(array("nombre"=>"Personas","data"=>$val1));
                }
                else{
                    $arma=Arma::find($data['valores']);
                    foreach($c->alumnos as $alumno){
                        if(count($alumno->arma_personas)>0){
                        if($alumno->arma_personas->last()->arma->id==$data['valores']){
                            $val1=$val1+1;
                        }
                        }
                    }
                    $series=array(array("nombre"=>$arma->nombre,"data"=>$val1));
                }
            }


        }*/
        if(Auth::user()->tipo=='0'){
            $direcciones = Direccion::all();
            $direcciones = array(0 => 'TODAS') + $direcciones->lists('nombre', 'id');
            $escuelas = Escuela::all();
            $escuelas = array(0 => 'TODAS') + $escuelas->lists('nombre', 'id');
        }elseif(Auth::user()->tipo=='1'){
            $direcciones=Auth::user()->escuela->direccion()->get();
            $escuelas=Escuela::where('direccion_id','=',$direcciones[0]->id)->get();
            $direcciones = $direcciones->lists('nombre', 'id');
            $escuelas = array(0 => 'TODAS') + $escuelas->lists('nombre', 'id');
        }else{
            $direcciones=Auth::user()->escuela->direccion()->get();
            $direcciones = $direcciones->lists('nombre', 'id');
            $escuelas=Escuela::where('id','=',Auth::user()->escuela_id)->get();
            $escuelas = $escuelas->lists('nombre', 'id');
        }

        if ($data['direccion_id'] == 0) {
            $escuelas = Escuela::all();
            $escuelas = array(0 => 'TODAS') + $escuelas->lists('nombre', 'id');
        }
        $tipos = array('0' => 'TODOS', '1' => 'CAPACITACIÓN', '2' => 'ESPECIALIZACIÓN', '3' => 'FORMACIÓN', '4' => 'PERFECCIONAMIENTO', '5' => 'EDUCACIÓN REGULAR');
        $cursos = array(0 => 'TODOS') + $cursos_reporte->lists('nombre', 'id');
        $atributos = array('0' => 'TODOS', '1' => 'GÉNERO', '2' => 'NACIONALIDAD', '3' => 'CALIFICACIÓN');

        return view('reporte.general', array('series'=>$series,'val1' => $val1, 'val2' => $val2, 'val3' => $val3, 'cursos_reporte' => $cursos_reporte, 'valores' => $valores, 'atributos' => $atributos, 'cursos' => $cursos, 'tipos' => $tipos, 'direcciones' => $direcciones, 'escuelas' => $escuelas, 'action' => $action, 'data' => $data, 'i' => '0', 'title' => $title, 'page_title' => $page_title, 'nav1' => $nav1, 'nav2' => $nav2
        , 'cursos' => $cursos
        ));

    }

    public function postEscuelas(Request $request)
    {
        $data = $request->all();
        if ($data['direccion_id'] == 0) {
            $escuelas = Escuela::all();
        } else {
            $escuelas = Escuela::where('direccion_id', '=', $data['direccion_id'])->get();
        }
        $html = "<option selected value='0'>TODAS</option>";
        $html_cursos = "<option selected value='0'>TODOS</option>";
        if (count($escuelas) > 0) {
            foreach ($escuelas as $escuela) {
                $html .= "<option value='" . $escuela->id . "'>" . $escuela->nombre . "</option>";
                $cursos = Curso::where('escuela_id', '=', $escuela->id)->get();
                if (count($cursos) > 0) {
                    foreach ($cursos as $curso) {
                        $html_cursos .= "<option value='" . $curso->id . "'>" . $curso->nombre . "</option>";
                    }
                }
            }
        }
        echo json_encode(array('opciones' => $html, 'cursos' => $html_cursos));

    }

    public function postCursos(Request $request)
    {
        $data = $request->all();
        if ($data['escuela_id'] == 0) {
            $cursos = Curso::all();
        } else {
            $cursos = Curso::where('escuela_id', '=', $data['escuela_id'])->get();
        }
        $html_cursos = "<option value='0'>TODOS</option>";
        if (count($cursos) > 0) {
            foreach ($cursos as $curso) {
                $html_cursos .= "<option value='" . $curso->id . "'>" . $curso->nombre . "</option>";
            }
        }
        echo json_encode(array('cursos' => $html_cursos));
    }

    public function postTipos(Request $request)
    {
        $data = $request->all();
        $html_cursos = "<option value='0'>TODOS</option>";
        if ($data['direccion_id'] == 0) {
            $cursos = Curso::all();
        } else {
            if ($data['escuela_id'] == 0) {
                $escuelas = Escuela::where('direccion_id', '=', $data['direccion_id'])->get();
                foreach ($escuelas as $escuela) {
                    if (count($escuela->cursos) > 0)
                        foreach ($escuela->cursos as $curso) {
                            if (($data['tipo'] == 1 && count($curso->curso_capacitacion) > 0) || ($data['tipo'] == 2 && count($curso->curso_especializacion) > 0) || ($data['tipo'] == 3 && count($curso->curso_formacion) > 0) || ($data['tipo'] == 4 && count($curso->curso_perfeccionamiento) > 0) || ($data['tipo'] == 5 && count($curso->curso_educacion_regular) > 0))
                                $html_cursos .= "<option value='" . $curso->id . "'>" . $curso->nombre . "</option>";
                        }
                }
                return json_encode(array('cursos' => $html_cursos));

            } else {
                $cursos = Curso::where('escuela_id', '=', $data['escuela_id'])->get();
            }
        }

        if (count($cursos) > 0) {
            foreach ($cursos as $curso) {
                if ($data['tipo'] == 0 || ($data['tipo'] == 1 && count($curso->curso_capacitacion) > 0) || ($data['tipo'] == 2 && count($curso->curso_especializacion) > 0) || ($data['tipo'] == 3 && count($curso->curso_formacion) > 0) || ($data['tipo'] == 4 && count($curso->curso_perfeccionamiento) > 0) || ($data['tipo'] == 5 && count($curso->curso_educacion_regular) > 0))
                    $html_cursos .= "<option value='" . $curso->id . "'>" . $curso->nombre . "</option>";
            }
        }
        return json_encode(array('cursos' => $html_cursos));
    }

    public function postAtributos(Request $request)
    {
        $data = $request->all();
        $html_cursos = "<option value='0'>TODOS</option>";
        if ($data['atributo'] == 1)
            $valores = array('H' => "HOMBRE", "M" => "MUJER");
        if ($data['atributo'] == 2) {
            $valores = array('N' => "NACIONALES", "E" => "EXTRANJEROS");
        }
        if ($data['atributo'] == 3)
            $valores = array('A' => 'APROBADO', 'R' => 'REPROBADO', 'O' => 'OTROS');
        if ($data['atributo'] == 4) {
            $valores=Arma::all();
            $valores=$valores->lists('nombre','id');
        }
        if($data['atributo'] == 5){
            $grados=Grado::todos();
            $valores=array();
            foreach($grados as $grado){
                $valores=array_add($valores,$grado->id,$grado->nombre);
            }
        }
        if (isset($valores)) {
            foreach ($valores as $key => $value) {
                $html_cursos .= "<option value='" . $key . "'>" . $value . "</option>";
            }
        }
        echo json_encode(array('valores' => $html_cursos));
    }

    public function getSeguimiento($msg = null)
    {
        $title = 'Personas | Búsqueda ';
        $page_title = 'Búsqueda de Persona';
        $nav1 = 'Personas';
        $nav2 = 'Búsqueda';
        $persona = new Persona();
        $action = 'ReporteController@postSeguimiento';
        return view('reporte.seguimiento', array('action' => $action, 'persona' => $persona, 'msg' => $msg, 'i' => '0', 'title' => $title, 'page_title' => $page_title, 'nav1' => $nav1, 'nav2' => $nav2
        ));
    }

    public function postSeguimiento(Request $request)
    {
        $data = $request->all();
        $persona = new Persona();
        $persona->nombres = $data['nombres'];
        $persona->apellidos = $data['apellidos'];
        $persona->identificacion = $data['identificacion'];
        $title = 'Personas | Búsqueda ';
        $page_title = 'Búsqueda de Persona';
        $nav1 = 'Personas';
        $nav2 = 'Búsqueda';
        $personas = Persona::where('nombres', 'like', '%' . $data['nombres'] . '%')
            ->where('apellidos', 'like', '%' . $data['apellidos'] . '%')
            ->where('identificacion', 'like', '%' . $data['identificacion'] . '%')
            ->get();

        $action = 'ReporteController@postSeguimiento';
        return view('reporte.seguimiento', array('action' => $action, 'persona' => $persona, 'personas' => $personas, 'i' => '0', 'title' => $title, 'page_title' => $page_title, 'nav1' => $nav1, 'nav2' => $nav2));

    }

    public function getVer($id)
    {
        $title = 'Personas | Seguimiento ';
        $page_title = 'Información de Persona';
        $nav1 = 'Personas';
        $nav2 = 'Seguimiento';
        $persona = Persona::find($id);
        $i = 0;
        return view('reporte.view', array('i' => $i, 'persona' => $persona, 'title' => $title, 'page_title' => $page_title, 'nav1' => $nav1, 'nav2' => $nav2));
    }

    public function getCursoLista($id)
    {
        $title = 'Reportes | Lista de Alumnos  ';
        $page_title = 'Lista de Alumnos';
        $nav1 = 'Reportes';
        $nav2 = 'Lista de Alumnos';
        $curso_personas = CursoPersona::where('curso_id', '=', $id)->get();
        $curso = Curso::find($id);
        return view('reporte.curso_list', array('i' => '0', 'title' => $title, 'page_title' => $page_title, 'nav1' => $nav1, 'nav2' => $nav2
        , 'curso_personas' => $curso_personas, 'curso' => $curso
        ));
    }
    public function getTitulos()
    {
        $title = 'Reportes | Lista de Títulos  ';
        $page_title = 'Lista de Títulos';
        $nav1 = 'Reportes';
        $nav2 = 'Lista de Títulos';
        $titulos = Titulo::all();
        return view('reporte.titulo_list', array('i' => '0', 'title' => $title, 'page_title' => $page_title, 'nav1' => $nav1, 'nav2' => $nav2
        , 'titulos'=>$titulos
        ));
    }

}